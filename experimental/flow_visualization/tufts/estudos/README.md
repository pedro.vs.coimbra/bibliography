---
title: Revisão Bibliográfica Mini-Tufos
author: Pedro Vinícius
output: pdf_document
---

# Chapter 9: Tufts, Flow Visualization Handbook
## Fluorescent Mini Tufts
The fishing line is recommended for large models.
Is spatial resolution important for us?

# Materials
* 0.006 in: Fishing line or sewing thread of 0.0007.
* Ordinary white textile material is enough for 0.006 in-dia
* Brighter: Sandoz, Inc. Leucophor EFR Liquid.

# Methods
* Concentration of 1% of Leucophor in water with 2% acetic acid add.
* Wound onto an open wire reel.
* Immerse the reel in the solution for 15 min. at 180ºF with frequent stirring.
* Rinsed the nylon in fresh water.
* Dried for several hours.
* Wipe the material to remove the loose of fluorescent powder.
* Wrap the airfoil with a continuous length of tuft
* Then cut free the tufts

## Attachment with liquid adhesive
### Materials
* Adhesive: nitrocellulose lacquer
* Wipe the surface with solvent, use tissues and chlorinated hydrocarbons such as trichloroethylene. Do not use alcohol.
fixtures luminárias
* airplane construction or DUCO Household Cement
* 1:1 with solvent such as acetone, methyl ethyl ketone.
* small quantity of paint pigment
* Hypodermic syringe, with half of the solution
* 22-gauge needle

### Methods
* Apply the material with the syringe
* The sharpened tip must be broken from the 22-gauge needle
* After dried (30 min), the tufts should be cut free

\newpage
# Flow Visualization Of Secondary Currents On Spiral Separators
Read in Abril 20th 2019

## Results
### Visualisatlon by Fluorescent Mini-Tufts
Monofilament nylon fibre with a diameter of 104 $\mu m$ was used as the source of the tufts.  The nylon was treated with a commercial fabric brightener (Leucophor P.A.D.) which renders it fluorescent in UV light. A solution of 1% Leucophor was heated to 80 ° C, the nylon immersed for about 15 minutes and then dried. This fixes the brightener to the nylon fibre allowing it to be used underwater. Before use, kinks in the fibres were removed by taping them under tension to a frame and placing them in a oven at 180 ° C for 30 seconds.  The tuft length used should be small compared to the expected radius of curvature of the stream lines [16], and some preliminary experimentation showed that a length of approximately 20 mm was suitable. Following Stinebring and Treaster [18] strands of nylon fibre longer than required were taped to the trough at both ends with masking tape, the fibres being aligned tangentially to the arc lines drawn on the trough. A droplet of glue (Bostick) was applied to the fibre at the selected grid points (the intersection of the upstream radial line and the arc lines) by means of a pointed probe. Very small amounts of glue were applied using this method, resulting in minimum interference to the flow at the trough base. Once the glue had dried, the tufts were cut to length and the masking tape peeled off. No problems were experienced with tufts tearing free even at high flow rates, or in the presence of solids.  The rig was started up in closed circuit using water. Under natural illumination the tufts were completely invisible. On illuminating the tufts with UV light, they became easily visible as fluorescent purple lines. Best fluorescent was achieved using a 365 nm wavelength UV source, in this case a readily available 20 W "black-light blue" fluorescent tube which was suspended over the trough. Flow visualisation was carried out at flow rates of 2, 4 and 6 m3/hr.  Examination of the directions taken up by the tufts indicated that those in the outer trough were deflected from the original tangential direction, aligning themselves slightly inwards from the radius of curvature of the spiral as indicated by the arc lines. The deviation of the flow in this region was approximately 5 degrees. In the inner trough the deviation was very much less apparent. No significant difference could be seen in the direction taken up by the tufts when the flowrate was varied, the visualisation at a flow rate of 4 m3/hr is 

It can be seen that the tufts in the outer trough are fluttering as a result of turbulence. What was not successfully photographed but could be seen over a period of visual observation, was an apparent demonstration of the outward flow near the fluid free surface required by continuity. The tufts could be seen to be occasionally deflected outwards from the arc lines.  This outward deflection was in addition to the normal fluttering due to turbulence seen in Figure 2, and was estimated to be also of the order of 5 degrees. It was assumed that the tufts, especially in the outer trough region were subject to some occasional turbulent lift towards the free surface where the outward secondary flow exists.  It was considered that the apparent absence of a significant deviation of the tufts from the primary flow direction in the inner region of the trough could be attributed to their stiffness and weight. In addition, a tuft diameter of 104 $mu m$ is approximately l/lO of the flow depth in the inner region, causing the tufts to be influenced to some extent by the outward component of the secondary flow if Holland-Batt's estimate of flow reversal at a fractional depth of only 0.12 to 0.14 is correct. In order to assess the effect of tuft diameter, the original tufts were placed with 20/~m diameter tufts which were rendered fluorescent as before. Identical results were achieved, the finer tufts taking up similar positions to those previously observed, while proving considerably harder to handle and photograph 

#### Meterials
* Monofilament nylon fibre (104 $mu m$ din)
* Brightner (Leucophor P.A.D)

#### Methods
* 1% Leucophor heat to 80º C
* Imerse the nylon for 15 minunes then dried
* Put the tufts under tension to a frame and placing them in a oven at 180º C for 30 seconds

### Visualisation by Dye Injection
The small deflections observed using the tufts, together with the difficulties in photographing them for analysis made investigation of an alternative method of flow visualisation worthwhile. Dye injection was chosen, despite the problems noted above, on the grounds of simplicity and the fact that the flow at least in the inner part of the trough is laminar.  The dye used was a saturated solution of potassium permanganate which gave a clearly visible purple thread viewed against the yellow spiral trough. To avoid build up of dye in the water, the spiral was operated in open circuit. The dye was introduced to the base of the trough by means of 1 mL syringes fitted with 0.5 mm (o.d) hypodermic needles. The syringes were mounted below the spiral trough, and the needles were pushed up through small holes drilled in the fibreglass moulding, through the polyurethane trough lining and fixed in position flush with the trough base. Care had to be taken to ensure that the injected dye had no velocity component perpendicular to the trough surface which would interfere with the main flow [16]. To allow hands-free multi=point visualisation, two small peristaltic pumps were used to inject the dye, each pump having two outlets. Two of the outlets were fitted with tee pieces, to give a total of six injection points, located at the grid points previously used for the tufts. The pumps were set at their lowest delivery rates (0.5 mL/min), which provided an adequate amount of dye, while appearing to meet the injection velocity constraint.  The use of dye as a means of flow visualisation results in only a qualitative picture of the fluid flow pattern. Interpretation of the observed dye threads is difficult, the major shortcoming being that they provide pictures of either streaklines (filament lines) or pathlines, but do not make streamlines directly visible. A streamline is a curve that is every where tangential to the velocity vector, a streakline or filament line is the locus of all points that have passed through a particular fixed point of the flow field, and a pathline is the curve that a particular particle traces in the flow field as a function of time. The three lines only coincide when the flow is steady, when by definition each particle passing through a given point follows the same trajectory [16, 17]. In the case of the dye injection carried out here, the flow was unsteady, as can be deduced from the presence of surface travelling waves and the visualisation was demonstrating streaklines.  Figures 3 and 4 show the dye patterns at flow rates of 2 and 6 m3/hr. Note that dye was not injected along the innermost arc line which coincided with the inner wetted limit of the trough. Comparison of Figures 3 and 4 shows that altering the flow rate of water to the spiral makes no significant difference to the observed deviations of the primary flow at the trough base. In the outer region of the trough, photography of the dye streaks proved difficult due to the rapid diffusion of the dye by turbulence, only in Figure 3 can a very small dye streak be seen at the start of the outermost arc line. The dye patterns did demonstrate the existence of a secondary flow in the inner region of the trough not shown by the tufts. The flow in this region deviates by approximately 2 degrees from the primary 

---
## Referências
14. (Book) Nakayama Y., (ed.), Visualised Flow, Pergamon Press, p. 3, Oxford (1988). Tem no Instituto de Fisíca Teórica
15. Clayton B.R. & Massey B.S., Flow visualisation in water, a review of techniques, J.
Sci. Instrum., 44, 2 (1967).
16. Merzkirch W., Flow Visualisation, Academic Press (1974). 
18. Stinebring D.R. & Treaster A.L., Water tunnel flow visualisation by the use of 
fluorescent mini-tufts, Proc. Third Int. Sym. on Flow Visualisation, 65, (Sept. 6-9,1983). 

## New Words
  + Coal Carvão
  + Skew Inclinado
  + Bend Dobrar
  + Thread Fio
  + Slik Tal
  + Bulk Tamanho
  + Awkward Estranho
  + Trough Calha
  + Droplet Gotícula
  + Glue Cola
  + Fed Alimentado

## Vocabulary
  + Some simple flow visualisation experiments was carried out
  + Experimental studies have been succefully made of ...
  + Experimental os carried out using a 
---

# Qualitative And Quantitative Flow

## Results
The front-lighting method (B) is useful for visualizing liquid convection by using the surface tufts method.  In this method, the tufts, 20 mm long are seamed on a thin brass plate in a lattice form. The brass plate is painted black and set on the internal wall of the tank. Of course, the tufts traceability, such as time response, is not able to detect the instantaneous flow but the average flow pattern of liquid phase can be easily identified.

---
## New Words
+ Overall No geral
+ Lakes Lagos
---

# Study of the Unsteady Flow Features on a Stalled Wing
## New Words
* Unsteady Instável
* Airframe Armação-Aéra
* Shedding Derramamento
* Bluff Blefe
* Narrow Limitar
* Termed Denominado
* Led Conduziu
* Gathered Coletado
* Unswept Não varrido

## Vocabulary
* When combining the experimental information gathered
---
