---
title: Bibliographic Review of Protuberances
author: Pedro Vinícius
#geometry: margin=0.5cm 
output: pdf_document 
indent: true 
---
\newpage
(ND) - stands for not downloaded
(DD) - stands for downloaded

Heading
=======

# New
Aeroacoustic analysis of a wind turbine rotor with a sinusoidal leading edge.

# Vocabulary

# Ideas
 Hansen 2009, introduction well organized in each paragraph she pick up an author and explored their work, and present an opposite opinion when it exists.
\newpage

# Hydrodynamic Design of the Humpback Whale Flipper# €
## Info: Printed 07/08/2019
## Procedure 
They cut a beached whale's flipper in several sections and did a lot of mesuerments.
## Conclusions 
" The humpback whale is the most "acrobatic" of ballen whales. "

"The feeding behavior of humpbacks  is considered more energetically demanding than the skim feeding of vowhead whales (Dolphin, '87). However, faster swimming and individual foraging by finback whales was sugested vy Whitehead and Carlson to be less advantageuous for feeding success than maneuverability by groups of humpback whales."
 
## References <++>
## My text 
  " An answer to van Nierop, this characteristic of the humpback whale is unique, but it does not mean that it is not advantageuous, acctually it is the advantage of the humpbacks when compared to other baleens. This more menuevable behavior gives this whales a more successful feeding."

  As características aerodinâmicas das nadadeiras da baleia jubarte foram destacadas por Fish e Battle ('95), assim como suas características geométricas. Esses autroes, também destacaram, como essas características influenciam no comportamento desse animal na captura de sua presa. Por terem a maior nadadeira entro os cetáceos e protuberâncias estampadas no bordo de ataque as baleias jubarte apresentam alto desempenho hidrodinâmico, com isso seu comportamento alimentar  demanda mais energia, porém, é mais eficiente quando comparada ao comportamento de outras baleias que possum nadadeiras menores, e menos manobrabilidade.

* Colocar as referencias
* Palavras novas
\newpage# ¥

# Leading-edge Tubercles Delay Stall on Humpback Whale (Megaptera novaeangliae) Flippers# €
## Procedure 
  Wind tunnel; $Re=5.20 \times 10^5$; 
## Conclusions 
  They studied the effects of protuberances on an idealized Humpback whale flipper in a wind tunnel at a Reynolds number around $Re=5.20 \times 10^5$. Their results have shown a delay in stall angle by approximately 40% and increasing in lift and decreasing in drag. Besides that, the scalloped leading edge ameliorate the post stall characteristics. They also draw analogies between the protuberances and vortex generators.
## References 
* Taylor: "Summary report on vortex generators" (unable to download) 
* Miklosovic: " Effects of surface finish on aerodynamics performance of a represntative stabilizer"
* Bushnell: "Drag reduction in nature" (downloaded)
## My text 
  Procurar um contexto para colocar o resuminho acima. 
\newpage# ¥

# Effects of Leading-Edge Protuberances on Airfoil Performance# €
## Info: Reading date 10th of August of 2019
## Procedure 
  c - mean chord
  *Nomenclatura:* Number 4 0.5c and 8 0.25c and the letter amplitude S-0.025c, M 0.05c, L-0.12c.
  Water tunnel; $Re = 1.83 \times 10^5$; Flow vizualisation with tufts.
## References 
Watts: The Influence of Passive, Leading Edge Tubercles on Wing Performance - (ND)
Stein: Stall Mechanism Analysis of Humpback Whale Flipper Models - (ND)
Murray: Effects of Leading Edge Tubercles on a Representative Whale Flipper Model at Various Sweep Angles - (ND)
Tanner: A Method of Reducing the Base Drag of Wings with Blunt Trailing Edges - (ND)
Tombazis: A study of three-dimensional aspcts of vortex shedding from Blff Boddy with a Mild Geometric - (ND)
## My text 
  Johari et al. measured in a water tunnel at a reynolds of $Re = 1.83 \times 10^5$, the lift, drag, and pitching moments of modified airfoils with leading edge protuberances and compared with those of the baseline $64\subscript{4}-021$. They also carried out flow visualization using tufts revealing that the flow over the protuberances remained attached well past the anlge of attack, and the flow separation at hte leading edge of modified foils occurrd mainly in the troughs between the adjacente protuberances.
\newpage# ¥

# How Bumps on Whale Flippers Delay Stall: An Aerodynamic Model# €
# Info: Reading date:07/07/2019
## My text 

Van Nierop et al 2008, explain in their letter using a theorical model the mechanisms that provid the enhanced characteristcs of the wavy airfoils, in oposition to previous work (who?) they do not believe that the tubercles may act as vortex generator device, because both the wavelength and amplitude of the protuberances are much larger than the boundary layer thickness. Instead, they propose that the tubercles alter the pressure distribution on the wing such that the separation of the boundary layer is delayed behind the peaks and this phenomena leads to a gradual onset of stall and larger stall angle. 


"Figure 2 shows pressure contours for the top surface of a bumpy wing. Since neighboring bumps and troughs have similar thicknesses but different chords, the same pressure difference must be overcome over a shorter distance be- hind a trough than behind a bump. Hence the pressure gradient is more adverse behind troughs than behind bumps, and separation occurs first behind troughs." (Colocar isso no texto)
\newpage# ¥

# The Effect Of Leading Edge Tubercle Geometry On The Performance Of Different Airfoils# €
## Info: Reding date 11/09/2019
## Intro
  Really well organized, should be used as a model for mine.
## Procedure 
  * Wind tunnel
  * Drag and lift forces for eight different airfoils
  * Angle $-4º \leq \alpha \leq 30º$
  * $Re = 4370$, Hydrogen bubble visualisation, Water tunnel.
  * Force experiments, $Re = 120000$
## Results
  * It seems that there are a optimum tubercle geometry for each profile
## Conclusions 
  * Best tubercles configuratios - smallest amplitude - narrowest wavelength.
  * The naca 65-021, show a notable differece, higher lift-to-drag ratio.
  * Smooth out the pitching moment characteristcs of the airfoils
## References 
Watts: The Influence of Passive, Leading Edge Tubercles on Wing Performance (ND)
## My text 
 An experimental investigation udertaken by Hansen 2009 compared two differente profiles, a NACA 65-021 and a NACA 0021, the tubercles seems to be more benefitial for the 65-021 airfoils than for the NACA 0021 airfoils, showing a higher lift-to-drag ratio for the 65. For both profiles the tubercles smooth out hte pitching moment  characteristcs. 
\newpage# ¥

# Marine Applications of the Biomimetic humpback Whale Flipper # €
## References 
Fish: Hydrodynamic flow control in marine mammals
Fish: Passive and active flow control y swimming fishs and mammals
Fish: The humpback whale's flipper: Application of bio-inspired tubercle technology
Goruney: Flow past a delta wing with a sinusoidal leading edge: Near-surface topology and flow structure
Hansen: Performance variations of leading-edge tubercles for distinct airfoil profiles
Hansen: Reduction of flow induced tonal noise through leading edge tubercle modifications
Howle: WhalePower Wenvor blade
Murray: Experimental evaluation of sinusoidal leading edges
Murray: Stall delay by leading edge tubercles on humpback whale flipper at various sweep angles
Murray: Effect of leading edge tubercos on maine tidal turbine blades
Owen: Suppression of Karman vortex shedding
Ozen: Control of votical structures on a flapping wing via a sinusoidal leading-edge
Pedro: Numerical study of stall delay on hmpvack halw flippers
Saadat:Explanation of the effects os leading-edge tubercles on the aerodynamics of airfoils and finite wings
Stanway: Hydrodynamic effects of leading-edge tubercles on control surfaces and flapping foil propulsion.
Weber: Lifti, drag, and cavitation onset on rudders with leading-edge tubercles.
Weber: Computational evaluation of the performance of lifiting surfaces with leading edge protuberances
Wind Energy Istitute of Canada: WhalePower Tubercle Blade Performace Test Report
## My text 
Muito interessante para conferir aplicações das protuberancias. E algumas referencias.

# Aerodynamic Performance and Surface and Surface Flow Structures of Leading-Edge Tubercled Tapered Swept-Back Wings
## Info: Reading date: Printed on 21 of June 2019
## Intro <++>
## Procedure <++>
## Conclusions <++>
## References <++>
## My text <++>
\newpage

# A large-eddy simulation on a deep-stalled aerofoil with a wavy leading edge
## Info: Reading date: 26/06/2019 - 02:29 AM Printed on 21 of June 2019
## Intro 
As baleias tem o record de maior nadadeira ( acrescentar o de maior rota). Sua alimentação exige alta manoeuvrability.  Para realizar seus saltos para fora da água (U-turns) imprimem um alto angulo de ataque nas suas nadadeiras. "It has been reported in previous literature that, as strakes, the tubercles may not increase the masimum lift of the flippers but they help to maintain a certain level of lift while the whale is turning a high AoA". 

" In cases with a wing tip, it was involved routinely found that at high angles of attack the WLEs manage to confine the wing-tip, flow separation to the outbord of the wing resulting in an increased the stall angle and maximum lift coefficient. Guerreiro $ Sousa reported that the performance of an undulated wing increased with its aspect ration. On the other hand, in the cases with an infinite span, the results show that WLEs, tend to result in an earlier stall than conventional straight leading edges (SLEs) although the stall process is less abrupt and the post-stall performance (lift-to-drag ratio) may be higher(Johari; Hansen etc)"

"The effect of Reynolds number has been studied and the results in general agreed that the WLEs tend to provid smoother stall characteristics regardless of the Reynolds numer or wing configuration wing."

" One of the common characteristics of undulated wings is to promote a higher pressure at hte peaks and a lower pressure at the troughs. It has been shown that the low pressure at the troughs is the consequence of an accelerated flow channelled between two adjacent peaks. Such pressure variations across the span promotes a secondary cross-flow that results in a stronger flow exchange/mixing. There exists diverse observations with regard the spanwise pressure distribution at high angles of attack."

"There exists a consensus among Johari et al. (2007), Hansen et al. (2011), Weber et al. (2011), Favier et al. (2012), Rostamzadeh et al. (2014) and Skillen et al.  (2015) that the flow past a WLE seems to easily separate at the troughs while it remains attached at the peaks. The flow past a trough which starts with a low pressure encounters a large adverse pressure gradient and is therefore prone to an early separation. Studies at low and medium Reynolds numbers has shown that the early flow separation in the trough area is followed by a reattachment further downstream forming a laminar separation bubble (LSB). At high angles of attack, some of the LSBs tend to break down due to the high adverse pressure gradient there, while others seem to group together. These events leave footprints of the low-pressure pockets near the leading edge. Hansen et al. (2016) studied the origin of LSB in the trough area at a low Reynolds number (Re = 2230) by using particle image velocimetry (PIV) and a steady-state Reynolds-averaged Navier–Stokes (RANS) calculation. Due to the very low Reynolds number, the LSB, which they refer to as the vorticity canopy, covered almost the entire chord length. They compared the LSB zone to an ‘owl face’ of the first kind (Perry & Hornung 1984)".

## Procedure 
CFD

## Results
They show that the flow from the peaks bends towards the LSBs (low-pressure spots).
The protuberances destroy the vortex shedding.


## Conclusions <++>
## References <++>
## My text 
 They used a Reynolds number of $Re_{\infty}1.2 \times 10^5$ and angle of attack $\alpha = 20$º. They confirmed an increase in lift and reduction in drag and also attributed this benefits to three major events: (i) the appearance of large low-pressue zone near the leading edge created by the laminar separation bubbles (LSBs); (ii) the reattachment of flow behind the LSBs resulting in a decreased volume of the rear wake; and, (iii) the deterioration of von-Kármán (periodic) vortex shdding due to the breakdown of spanwise coherent structures.

\newpage

# An experimental study onflow separation control of hydrofoils with leading-edge tubercles at low Reynolds number
## Info: Reading date: 08/07/2019 Printed on 21 of June 2019
## Intro <++>
## Procedure 
* Exprerimental
* Re = 1.4 $\times$ 10^4
* PIV
* Paricle-steak visualizations
## Conclusions 

## References <++>
## My text <++>
\newpage

# A numerical investigation into the effects of Reynolds
## Info: Reading date: Printed on 21 of June 2019
## Intro <++>
## Procedure <++>
## Conclusions <++>
## References <++>
## My text <++>

To investigate the effects of Reynolds number on the flow mechanism induced by  tubercled foils {\bf Rostamzadeh et al} carried out a numerical study, their simulations shown that in contrast to the transitional flow regime, where the unmodified NACA-0021 undergoes a sudden loss of lift, in the turbulent regime, the baseline foil experiences gradual stall and produces more lift than the tubercled foil.
The results of flow topology analyses revealed a number of common, as well as different flow features in the studied flow regimes. One primary feature observed in both cases was the presence of spanwise pressure gradients in the vicinity of the leading edge. The cyclic variation in pressure gradient is induced by the leading- edge tubercles and results in the highest suction peak to occur in the trough spanwise location. The pressure difference between the trough and the peak drives the flow to move towards the trough. On the other hand, the abrupt change in the flow direction in this region leads to strong velocity gradients that account for the turning of the already-present spanwise vorticity within the boundary layer towards the streamwise and transverse directions. This mechanism operates in both flow regimes and is identified as Prandtl’s secondary flow of the first type.

 The numerical simulation results indicate that   This observation highlights the importance of considerations regarding the Reynolds number effects and the stall characteristics of the baseline foil, in the industrial applications of tubercled lifiting bodies.  
\newpage

# Cavitation on hydrofoils with leading edge protuberances
## Info: Reading date: Printed on 21 of June 2019
## Intro <++>
## Procedure <++>
## Conclusions <++>
## References <++>
## My text <++>
\newpage

# Detailedflow measurement of thefield around tidal turbines with andwithout biomimetic leading-edge tubercles
## Info: Reading date: Printed on 21 of June 2019
## Intro <++>
## Procedure <++>
## Conclusions <++>
## References <++>
## My text <++>
\newpage

# Direct numerical simulations of the flow aroundwings with span wise waviness at a very low Reynolds number
## Intro <++>
## Procedure <++>
## Conclusions <++>
## References <++>
## My text <++>
\newpage

# Effect of Leading Edge Tubercles on Transonic Performance of Airfoils
## Intro <++>
## Procedure <++>
## Conclusions <++>
## References <++>
## My text <++>
\newpage

# Effect of the wavy leading edge on hydrodynamic characteristics for flowaround low aspect ratio wing
## Intro <++>
## Procedure <++>
## Conclusions <++>
## References <++>
## My text <++>
\newpage

# Evolution of the streamwise vortices generated
## Info: Reading date: 05/07/2019 Printed on 21 of June 2019
## Intro <++>
## Procedure 
Reynolds number of 2230, based on the foil chord.
## Conclusions 
The combined experimental and numerical studies have identified the salient features of the flow structure produced by a NACA 0021 foil with tubercles at a Reynolds number of 2230. The presence of the tubercles leads to an increased flow velocity along the trough, and a larger adverse pressure gradient. This in turn causes boundary-layer separation to occur in the trough at a low angle of attack. These findings are consistent with previous research, where the larger adverse pressure gradient in the trough region was attributed to the relatively short chord length at this spanwise location (van Nierop et al. 2008). Hence, the same pressure difference needs to be overcome over a shorter distance behind a tubercle trough relative to a tubercle peak (van Nierop et al. 2008). Previous studies have also identified the presence of a spanwise pressure gradient (Skillen et al. 2014), and the results presented here indicate that this is most significant in the region of the leading edge, where the minimum suction peak occurs. It has been shown that the spanwise pressure gradients, set up by the tubercles, interact with the vorticity distribution, leading to a complex flow pattern.

 The current work also shows for the first time that the counter-rotating streamwise vortices lift away from the surface at a certain chordwise location, corresponding to the large foci in the surface streamline (shear stress) pattern, the locations of which are governed by the angle of attack of the foil. At this point, the flow field becomes highly unsteady and the distribution of vorticity in the region of the primary vortices varies with time, a feature that has not been previously identified. **Perguntar panta se da pra incluir essa parte pq eu não sei direito o que ta sendo falado**.

## References 
van Nierop 2008
Custodio 2007
Johari 2007
 Check later
 **Perkins**
 **Perry**

## My text 

 Hansen et al 2016, combined experimental and numerical studies and have identified the salient features of the flow structure produced by a NACA 0021 foil with tubercles at Reynolds number of 2230. They have shown that the tubercles leads to an increased flow velocity along the trough, and a lager adverse pressure gradinet.  The findings of this authors are cosistent with previous research of van Nierop 2008 et al., where they attributed the larger adverse pressure gradinet in the trough region to the shorter chord length at this spanwise location. Van Nierop et al also explain that  the same pressure difference needs to be overcome over a shorter distance behind a tubercle trough relative to a tubercle peak.

spanwise pressure gradient (nossos futoros trabalhos com mini-tufos podem mostrar isso?)
\newpage

# Experimental investigation of wavy leading edges on rod-aerofil interaction noise
## Intro <++>
## Procedure <++>
## Conclusions <++>
## References <++>
## My text <++>
\newpage

# Experimental Study on Bio-Inspired Wings with Tubercles
## Intro <++>
## Procedure <++>
## Conclusions <++>
## References <++>
## My text <++>
\newpage

# Flow structure modifications by leading-edge tubercles on a 3D wing
## Intro <++>
## Procedure <++>
## Conclusions <++>
## References <++>
## My text <++>
\newpage

# Formation of vortices on a tubercled wing, and their effects on drag
## Info: Reading date: Printed on 21 of June 2019
## Intro <++>
## Procedure <++>
## Conclusions <++>
## References <++>
## My text <++>
\newpage

# Mimicking the humpback whale: An aerodynamic perspective
## Intro <++>
## Procedure <++>
## Conclusions <++>
## References <++>
## My text <++>
\newpage

# Numeriacal Investigations of Hydrodanamic produberance of hydrofoil with
## Intro <++>
## Procedure <++>
## Conclusions <++>
## References <++>
## My text <++>
\newpage

# Numerical analysis of broadband noise reduction
## Intro <++>
## Procedure <++>
## Conclusions <++>
## References <++>
## My text <++>
\newpage

# Numerical evaluations of the effect of leading-edge
## Intro <++>
## Procedure <++>
## Conclusions <++>
## References <++>
## My text <++>
\newpage

# Numerical Simulations of Low-Reynolds-Number Flow Past Finite Wings with Leading-Edge Protuberances
## Info: Reading date: Printed on 21 of June 2019
## Intro <++>
## Procedure <++>
## Conclusions <++>
## References <++>
## My text <++>
\newpage

# Performance effects of a single tubercle terminating at a swept wing’s tip
## Intro <++>
## Procedure <++>
## Conclusions <++>
## References <++>
## My text <++>
\newpage

# Performance Variations of Leading-Edge Tubercles_for Distinct Airfoil Profiles
## Intro <++>
## Procedure <++>
## Conclusions <++>
## References <++>
## My text <++>
\newpage

# The Performance of a Vertical Axis Wind Turbine With Camber and Tubercle Leading Edge as Blade Passive Motion Controls
## Intro <++>
## Procedure <++>
## Conclusions <++>
## References <++>
## My text <++>
\newpage

# The Tubercles on Humpback Whales’ Flippers Application of
## Intro <++>
## Procedure <++>
## Conclusions <++>
## References <++>
## My text <++>
\newpage

---
# Anotar número de reynolds
# Anotar frases chave, palavras chave de ligação
# The template is on register t
## Intro <++>
## Procedure <++>
  * $Re = $
  * <++> Tunnel
  * Angle
## Conclusions <++>
## References <++>
## My text <++>
---

Aeroacoustic analysis of a wind turbine rotor with a sinusoidal leading edge 
Aerodynamic Performance and Surface and Surface Flow Structures of Leading-Edge Tubercled Tapered Swept-Back Wings 
A large-eddy simulation on a deep-stalled aerofoil with a wavy leading edge 
An experimental study onflow separation control of hydrofoilswith leading-edge tubercles at low Reynolds number 
A numerical investigation into the effects of Reynolds 
Application of Leading Edge Protuberance on Forward-swept Wind Turbine 
Cavitation on hydrofoils with leading edge protuberances 
Detailedflow measurement of thefield around tidal turbines with andwithout biomimetic leading-edge tubercles 
Direct numerical simulations of the flow aroundwings with span wise waviness at a very low Reynolds number 
EFFECT OF LEADING EDGE TUBERCLES ON AIRFOIL PERFORMANCE 
Effect of Leading Edge Tubercles on Transonic Performance of Airfoils 
Effect of the wavy leading edge on hydrodynamic characteristics for flowaround low aspect ratio wing 
Effects of Leading-Edge Protuberances on Airfoil Performance 
Evolution of the streamwise vortices generated 
Experimental investigation of wavy leading edges on rod-aerofil interaction noise 
Experimental study of the protuberance effect on the blade 
Experimental Study on Bio-Inspired Wings with Tubercles 
Flow structure modifications by leading-edge tubercles on a 3D wing 
Formation of vortices on a tubercled wing, and their effects on drag 
Hydrodynamic Design of the Humpback Whale Flipper 
Leading-edge_Tubercles Delay Stall on Humpback Whale (Megaptera novaeangliae) Flippers 
Mimicking the humpback whale: An aerodynamic perspective 
Numeriacal Investigations of Hydrodanamic produberance of hydrofoil with ... 
Numerical analysis of broadband noise reduction 
Numerical evaluations of the effect of leading-edge 
Numerical Simulations of Low-Reynolds-Number Flow Past Finite Wings with Leading-Edge Protuberances 
Performance effects of a single tubercle terminating at a swept wing’s tip 
Performance Variations of Leading-Edge Tubercles_for Distinct Airfoil Profiles # ¥
