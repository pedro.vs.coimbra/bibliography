---
title: Compiled of articles intros
author: Pedro Vinícius Souza Coimbra
output: pdf_document
---

# 1º The effect of leading edge tubercle geometry on the performance of different airfoils
## Read in Mach, 17 of 2019

## Intro
 
Tubercles are leading-edge, rounded protuberances that alter the flow-field around an airfoil. The 
presence of tubercles on the humpback whale flipper, for example, has been associated with the 
tight turning manoeuvres that it is required to make as part of its feeding ecology [1]. It has been 
postulated that tubercles give rise to the formation of stream-wise vortices, which enhance 
momentum exchange within the boundary layer [1]. This can lead to improvements in foil 
performance, such as delayed stall and higher maximum lift coefficient, allowing a smaller foil 
surface to be utilised [2]. Hence, drag at the cruise condition and weight of the wing can be reduced. 

## Notes
The use other profile (65-021)
Experimental (hydrogen bubbles

## New words
Troughs - Calhas
Stream-wise vortices - Ask Panta
Negligible - Insignificante
Incur - Incorrer
Narrowest - Mais estreito
Thickness - Espessra
Pitching moment - Ask Panta

# 2º Leading-edge tubercles delay stall on humpback whale (Megaptera novaeangliae) flippers
## Read in March, 17 of 2019

## Intro
The turning performance of aquatic animals is constrained by morphology with the mobility and flexibility of the body and the hydrodynamic characteristics and position of the control surfaces influencing the animal’s turning performance. The baleen whales of the cetacean suborder Mysticete are the largest of all animals. Much of the body is inflexible because of their specialized feeding system, which restricts their maneuverability. However, the humpback whale (Megaptera novaeangliae) is unusual compared to other mysticetes in its ability to undertake acrobatic turning maneuvers in order to catch prey. Humpback whales utilize extremely mobile, high-aspect-ratio flippers for banking and turning. Large rounded tubercles, shown in the photo- graphs of Fig. 1, along the leading edge of the flipper are morphological structures that are unique in this animal. The position, size and number of tubercles on the flipper suggest analogs with specialized leading-edge control devices asso- ciated with improvements in hydrodynamic performance on biological3 and engineered4 lifting surfaces. It has been hy- pothesized that these leading-edge tubercles can modify the hydrodynamic characteristics of the flipper to increase its effectiveness in turning.5,6 In this study, we report wind tun- nel measurements of the lift and drag, along with other pa- rameters, as a function of the angle of attack of idealized scale models of a humpback whale flipper with and without tubercles.  We constructed two scale models of a humpback pecto- ral flipper; one with and one without leading-edge tubercles.  strained by morphology with the mobility and flexibility of the body and the hydrodynamic characteristics and position of the control surfaces influencing the animal’s turning per- formance. The baleen whales of the cetacean suborder Mys- ticete are the largest of all animals. Much of the body is inflexible because of their specialized feeding system, which restricts their maneuverability. However, the humpback whale (Megaptera novaeangliae) is unusual compared to other mysticetes in its ability to undertake acrobatic turning maneuvers in order to catch prey.1,2 Humpback whales uti- lize extremely mobile, high-aspect-ratio flippers for banking and turning. Large rounded tubercles, shown in the photo- graphs of Fig. 1, along the leading edge of the flipper are morphological structures that are unique in this animal. The position, size and number of tubercles on the flipper suggest analogs with specialized leading-edge control devices asso- ciated with improvements in hydrodynamic performance on biological3 and engineered4 lifting surfaces. It has been hy- pothesized that these leading-edge tubercles can modify the hydrodynamic characteristics of the flipper to increase its effectiveness in turning.5,6 In this study, we report wind tun- nel measurements of the lift and drag, along with other pa- rameters, as a function of the angle of attack of idealized scale models of a humpback whale flipper with and without tubercles.

## New words
Constrained by - Restringida por
Ballen - Barbatanas

# 3º Performance Variations of Leading-Edge Tubercles for Distinct Airfoil Profiles
## Read in March, 19 of 2019
### Nota A

  ## 1.  Intro Tubercles are leading-edge rounded protuberances that
alter the flowfield around an airfoil. It has been suggested that
tubercles on the humpback whale (Megaptera novaeangliae) flipper
function as lift-enhancement devices, **ALLOWING FLOW TO REMAIN
ATTACHED FOR A LARGER RANGE OF ATTACK ANGLES, thus delaying stall
[1] and increasing CL max [2]**. This is considered an important
characteristic for the humpback whale, **WHICH MUST PERFORM TIGHT
TURNING MANEUVERS AS PART OF ITS FEEDING ECOLOGY [3–5]. The radius
of such a turn is inversely proportional to the amount of lift
generated [6]; therefore, any potential increase in maximum lift
coefficient would be desirable. Hence, a mo rphological adaptation
for delaying stall would be highly beneficial for the humpback
whale. Additionally, the energy expenditure required to achieve a
given lift coefficient could potentially be lower, since the maximum
lift coefficient for a given swimming speed would be higher**.
According to several researchers, the mechanism responsible for the
improvement in performance is the generation of streamwise vortices,
which **enhance momentum exchange within the boundary layer [1,2]**.
*Thus, there may be* a strong similarity between tubercles and other
vortex generating devices currently in use, such as strakes and
small delta wings [7]. Other mechanisms have also been suggested,
such as the elimination of spanwise stall progression through flow
compartmentalization [8].  **This passive modification to the
leading edge of a foil that delays stall and enables a higher
maximum lift coefficient to be achieved has many potential
applications for engineered lifting surfaces. Applications with
Reynolds numbers similar to those of the humpback whale [2] include
wind turbine blades [9], unmanned aerial vehicles [10], sailboat
centerboards [8], and boat rudders [11].**

## 2. Important topics
* Similarity between tubercles and vortex generating devices [7]

## 3. New words
Undertaken - Realizado
Rounded - Arredondado
Flipper - Nadadeira
Strakes - Vortex generating devices **Panta**
Sailboat - Borco a vela
Rudders - Lemes
Attached - ligado

## 4. Technical expressions
Spanwise stall - **I did not understand**
Enhance momentum exchange within the boundary layer - **I did not understand**
Streamwise vortices - **I did not understand**

## 5. Rich vocabulary
Thus, there may be - Assim, pode haver
It has been suggested that - Tem sido sugerido

## 6. My Intro
* Tubercles are leading-edge rounded protuberances that alter the flowfield around an airfoil.
* Flipper function as lift-enhancement devices
* Allowing flow to remain attached for a larger range of attack angles
To perform tight maneuvers these needed for thees animals ...
* Similarity between tubercles and vortex generating devices [7]
* Applications

# 4º The Tubercles on Humpback Whales’ Flippers: Application of Bio-Inspired Technology
## Read March 19, of 2019 parei em Use of the flipper
### Nota A+

# 1. Intro

  Synopsis: The humpback whale (Megaptera novaeangliae) is exceptional
among the large baleen whales in its ability to undertake aquabatic
maneuvers to catch prey. Humpback whales utilize extremely mobile,
wing-like flippers for banking and turning. Large rounded tubercles
along the leading edge of the flipper are morphological structures
that are unique in nature. The tubercles on the leading edge act as
passive-flow control devices that improve performance and
maneuverability of the flipper. Experimental analysis of finite wing
models has demonstrated that the presence of tubercles produces a
delay in the angle of attack until stall, thereby increasing maximum
lift and decreasing drag. Possible fluid-dynamic mechanisms for
improved performance include delay of stall through generation of a
vortex and modification of the boundary layer, and increase in
effective span by reduction of both spanwise flow and strength of
the tip vortex. The tubercles provide a bio-inspired design that has
commercial viability for wing-like structures. Control of passive
flow has the advantages of eliminating complex, costly,
high-maintenance, and heavy control mechanisms, while improving
performance for lifting bodies in air and water. The tubercles on
the leading edge can be applied to the design of watercraft,
aircraft, ventilation fans, and windmills.

## 2. Important topics

  * The flippers of the humpback whale are the longest of any cetacean
(Fish and Battle 1995). Length varies from 0.25 to 0.33 of total
body length (Tomilin 1957; Winn and Winn 1985; Edel and Winn 1978;
Fish and Battle 1995).

" Para o largest" Fish FE, Battle JM. 1995. Hydrodynamic design of the humpback whale flipper. J Morph 225:51–60.

"para o valor" Edel RK, Winn HE. 1978. Observations on underwater locomotion and flipper movement of the humpback whale motion and flipper movement of the humpback whale Megaptera novaeangliae. Mar Biol 48:279–87.

## 3. New words
Biomimicry - Biomimética
Overlap - Sobreposição
Cheetahs - Leopardo

## 4. Technical expressions
<++>

## 5. Rich vocabulary
* The focus of this article

## 5. My intro
The largest com referẽncias e o tamanho

# 5º Effects of Leading-Edge Protuberances on Airfoil Performance 
## Read March 19, of 2019 
### <++>

# 1. Intro

  The idea of examining the effects of leading-edge protuberances on
airfoil performance was inspired by previous work of marine
biologists who studied the morphology of humpback whales pectoral
flippers. *Despite* its large size and rigid body, the humpback
whale (Megaptera novaeangliae) is extremely maneuverable compared
with other species. Turning maneuvers are especially evident during
pursuit of prey. The agility of the humpback whale has been
attributed to its use of pectoral flippers [1,2]. Typical humpback
whale flippers are shown in Fig. 1.  Fish and Battle [1] report that
the flippers have large aspect ratios (6), and large-scale
protuberances are present on the leading edge.  The flipper has a
symmetric profile for the most part, with a rounded leading edge and
a sharp trailing edge. The flipper thickness ratio ranges from 0.20
to 0.28 of the chord length, with a mean value of 0:23c. The
thickness ratio decreases from midspan to the tip. The maximum
thickness point varies from about 0:2c at midspan to about 0:4c near
the tip. The cross section of the flipper has a profile similar to a
NACA 634-021 airfoil [1]. A cross section of the flipper is compared
with a NACA 634-021 airfoil in Fig. 2. The amplitude of leading-edge
protuberances ranges from 2.5 to 12% of the chord, with a spanwise
extent of 10–50% of the chord depending on the location along the
span. It has been hypothesized that the “bumpy” leading edge is used
for flow control [1] and/or drag reduction [3].  

## 2. Important topics

  * The flippers of the humpback whale are the longest of any cetacean
(Fish and Battle 1995). Length varies from 0.25 to 0.33 of total
body length (Tomilin 1957; Winn and Winn 1985; Edel and Winn 1978;
Fish and Battle 1995).

## 3. New words
Despite - Apesar

## 4. Technical expressions
<++>

## 5. Rich vocabulary
* The focus of this article

## 5. My intro
*Despite* its large size and rigid body, the humpback whale (Megaptera novaeangliae) is extremely maneuverable compared with other species. [1,2]

# 6º Hydrodynamic Design of the Humpback Whale Flipper 
## Read March 19, of 2019 
### <++>

# 1. Intro

  **The humpback whale, Megaptera nouaeangliae, has the longest
flipper of any cetacean. Flipper length varies from 0.25 to 0.33 of
the total body length**.  Flipper shape is long, narrow, and thin
although shape is variable among individuals. The humpback whale
flipper is also unique because of the presence of large
protuberances or tubercles located on the leading edge, which gives
this surface a scalloped appearance. Locations of the tubercles
correspond to the positions of the cartilages of the manus.  The
unusual length of the pectoral flippers has been hypothesized to
provide benefits to the humpback whale including the ability of
these whales to navigate in much shallower waters than other whales
use for thermoregulation as heat dissipaters because of the high
surface-to-volume ratio production of sound signals by slaps on the
water surface use of the light coloration on the underside of the
flipper in herding prey into the mouth use of the light coloration
as a cue to others on the orientation and movement of the whale
(Madsen and Herman, use of the flippers during mating increased
maneuverability. This latter function suggests that the flippers act
as biological hydroplanes. The humpback whale **is the most
"acrobatic"** of baleen whales.  This species is capable of
performing underwater somersaults. If the flippers of the humpback
are adapted for maneuverability, they should display a morphology
for high hydrodynamic performance. There are limited data on the
amount of streamlining displayed in appendages of cetaceans and its
affect on hydrodynamic performance.  A single cross-section cut from
the tip of a humpback whale flipper indicated a symmetrical
streamlined profile with a rounded leading edge and tapering
trailing edge.

## 2. Important topics

## 3. New words
shallower
somersaults
## 4. Technical expressions

## 5. Rich vocabulary
The present study was
## 5. My intro
