# Step 8
# 2D Burger's equation
# Convection and difusion in two dimensions

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm

nx = 41
ny = 41
nt = 10
sigma = 0.0009
dx = 2 / (nx - 1)
dy = 2 / (ny - 1)
nu = 0.01
dt = sigma * dx * dy / nu

x = np.linspace(0, 2, nx)
y = np.linspace(0, 2, ny)
X, Y = np.meshgrid(x, y)
u = np.ones((nx, ny))
v = np.ones((nx, ny))

u[int(0.5 / dy) : int(1 / dy + 1), int(0.5 / dx) : int(1 / dx + 1)] = 2
v[int(0.5 / dy) : int(1 / dy + 1), int(0.5 / dx) : int(1 / dx + 1)] = 3

# Ployt IC and BC
# fig = plt.figure(figsize=(11, 7), dpi=100)
# ax = fig.add_subplot(projection='3d')
# surf2 = ax.plot_surface(X, Y, u[:], cmap=cm.viridis)
# # plt.show()

# def compute_convection(u: list) -> list:

# Solution using array operations
for n in range(nt + 1):
    un = u.copy()
    vn = u.copy()
    # Diffusion terms
    diffusion_u = nu * (dt / dx**2 * (un[2:, 1:-1] - 2 * un[1:-1, 1:-1] + un[0:-2, 1:-1])) + nu * (dt / dy**2 * (un[1:-1, 2:] - 2 * un[1:-1, 1:-1] + un[1:-1, 0:-2]))
    diffusion_v = nu * (dt / dx**2 * (vn[2:, 1:-1] - 2 * vn[1:-1, 1:-1] + vn[0:-2, 1:-1])) + nu * (dt / dy**2 * (vn[1:-1, 2:] - 2 * vn[1:-1, 1:-1] + vn[1:-1, 0:-2]))

    # Convection terms
    # i = 1:-1 # Começa em i e vai até o último elemento.
    # i - 1 = 0:-2 # Começa no primeiro elemento do vetor e termina no penúltimo, por tanto vai de j-1 ao penúltimo
    convection_u = -un[1:-1, 1:-1] * (dt / dx * (un[1:-1, 1:-1] - un[0:-2, 1:-1])) - vn[1:-1, 1:-1] * (dt / dy * (un[1:-1, 1:-1] - un[1:-1, 0:-2]))
    convection_v = -un[1:-1, 1:-1] * (dt / dx * (vn[1:-1, 1:-1] - vn[0:-2, 1:-1])) - vn[1:-1, 1:-1] * (dt / dy * (vn[1:-1, 1:-1] - vn[1:-1, 0:-2]))

    u[1:-1, 1:-1] = un[1:-1, 1:-1] + convection_u + diffusion_u
    v[1:-1, 1:-1] = vn[1:-1, 1:-1] + convection_v + diffusion_v
    u[0, :] = 1
    u[-1, :] = 1
    u[:, 0] = 1
    u[:, -1] = 1
    v[0, :] = 1
    v[-1, :] = 1
    v[:, 0] = 1
    v[:, -1] = 1

fig = plt.figure(figsize=(11, 7), dpi=100)
ax = fig.add_subplot(projection="3d")
ax.plot_surface(X, Y, u[:], rstride=1, cstride=1, cmap=cm.viridis, linewidth=0, antialiased=True)
ax.plot_surface(X, Y, v[:], rstride=1, cstride=1, cmap=cm.viridis, linewidth=0, antialiased=True)
ax.set_zlim(1, 2.5)
ax.set_xlabel("$x$")
ax.set_ylabel("$y$")
plt.show()
