# Step 1
# 2D Linear convection

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm

nx = 20
ny = 20
nt = 50
dt = 0.01
dx = 2 / (nx - 1)
dy = 2 / (ny - 1)

x = np.linspace(0, 2, nx)
y = np.linspace(0, 2, ny)
X, Y = np.meshgrid(x, y)
u = np.ones((nx, ny))
v = np.ones((nx, ny))

u[int(0.5 / dy):int(1 / dy + 1), int(0.5 / dx):int(1 / dx + 1)] = 2
v[int(0.5 / dy):int(1 / dy + 1), int(0.5 / dx):int(1 / dx + 1)] = 2

# Ployt IC and BC
# fig = plt.figure(figsize=(11, 7), dpi=100)
# ax = fig.add_subplot(projection='3d')
# surf2 = ax.plot_surface(X, Y, u[:], cmap=cm.viridis)
# # plt.show()

# Solution using array operations
for n in range(nt + 1):
    un = u.copy()
    vn = u.copy()
    u[1:, 1:] = (un[1:, 1:] - un[1:, 1:]*(dt / dx * (un[1:, 1:] - un[1:, :-1]))) \
        - vn[1:, 1:]*(dt / dy * (un[1:, 1:] - un[:-1, 1:]))
    v[1:, 1:] = (vn[1:, 1:] - un[1:, 1:]*(dt / dx * (un[1:, 1:] - un[1:, :-1]))) \
        - vn[1:, 1:]*(dt / dy * (un[1:, 1:] - un[:-1, 1:]))
    u[0, :] = 1
    u[-1, :] = 1
    u[:, 0] = 1
    u[:, -1] = 1
    v[0, :] = 1
    v[-1, :] = 1
    v[:, 0] = 1
    v[:, -1] = 1

fig = plt.figure(figsize=(11, 7), dpi=100)
ax = fig.add_subplot(projection='3d')
surf2 = ax.plot_surface(X, Y, u[:], cmap=cm.viridis)
plt.show()
