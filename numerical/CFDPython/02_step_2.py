# Step 2 - 1D convection

# Inviscid Burgers equation
# This equation can create shocks from smooth I.Cs


# Step 1
import numpy as np
import matplotlib.pyplot as plt

nx = 41
nt = 20
dt = 0.025
dx = 2 / (nx - 1)

x = np.linspace(0, 2, nx)
u = np.ones(nx)

u[int(0.5 / dx):int(1 / dx + 1)] = 2  # Setting u = 2 between 0.5 and 1 (I.Cs)

# plt.plot(x, u)
# plt.show()

un = np.ones(nx)

for n in range(nt):
    un = u.copy()
    # for i in range(nx):
    for i in range(1, nx):
        u[i] = un[i] - un[i] * dt / dx * (un[i] - un[i - 1])

plt.plot(x, u)
plt.show()
