# CFL condition - Video lesson 9

# nx = 20
# nt = 50
# dt = 0.01
# vis = 0.1
# dx = 2 / (nx - 1)

# x = np.linspace(0, 2, nx)
# u = np.ones(nx)

# u[int(0.5 / dx) : int(1 / dx + 1)] = 2  # Setting u = 2 between 0.5 and 1 (I.Cs)

# un = np.ones(nx)

# for n in range(nt):
#     un = u.copy()
#     for i in range(1, nx - 1):
#         u[i] = un[i] + vis * dt / dx / dx * (un[i + 1] - 2 * un[i] + un[i - 1])

# plt.plot(x, u)
# plt.show()

import numpy  # numpy is a library for array operations akin to MATLAB
from matplotlib import pyplot  # matplotlib is 2D plotting library


def linearconv(nx):
    dx = 2 / (nx - 1)
    nt = 20  # nt is the number of timesteps we want to calculate
    c = 1
    sigma = 0.5
    dt = sigma * dx

    u = numpy.ones(
        nx
    )  # defining a numpy array which is nx elements long with every value equal to 1.
    u[
        int(0.5 / dx):int(1 / dx + 1)
    ] = 2  # setting u = 2 between 0.5 and 1 as per our I.C.s

    un = numpy.ones(
        nx
    )  # initializing our placeholder array, un, to hold the values we calculate for the n+1 timestep

    for _ in range(nt):  # iterate through time
        un = u.copy()  # copy the existing values of u into un
        for i in range(1, nx):
            u[i] = un[i] - c * dt / dx * (un[i] - un[i - 1])

    pyplot.plot(numpy.linspace(0, 2, nx), u)
    pyplot.show()


linearconv(20)
linearconv(41)
linearconv(61)
linearconv(85)

# Video Lesson 8 Anki deck

# Consistency: is a condition on the numerical scheme
#     -> The scheme must tend to the differenctial equation when the steps in time and space tend to zero.
# Stability: is a condition on the numerical solution
#     -> All errors must remain bounded when the iteration progresses
#        For finite values of delta t, delta x, errors
#        has to remain vounded when the number of time
#        steps tends to infinity.
# Convergence: is a condition on the numerical solution
# The numerical solution must tend to the exact solution of the mathematical
# model, when steps in t and x tend to zero (i.e. when the mesh is refined).

# Equivalence theorem of Lax
# For a well-posed inicial value problem and a consistent discretization
# scheme, stability is the necessary and sufficient condition for convergence.

