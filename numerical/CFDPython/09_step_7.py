# Step 1
# 2D Diffusion

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm

nx = 31
ny = 31
nt = 10
sigma = 0.25
dx = 2 / (nx - 1)
dy = 2 / (ny - 1)
nu = 0.05
dt = sigma * dx * dy / nu

x = np.linspace(0, 2, nx)
y = np.linspace(0, 2, ny)
X, Y = np.meshgrid(x, y)
u = np.ones((nx, ny))
v = np.ones((nx, ny))

u[int(0.5 / dy):int(1 / dy + 1), int(0.5 / dx):int(1 / dx + 1)] = 2

# Ployt IC and BC
# fig = plt.figure(figsize=(11, 7), dpi=100)
# ax = fig.add_subplot(projection='3d')
# surf2 = ax.plot_surface(X, Y, u[:], cmap=cm.viridis)
# # plt.show()

# Solution using array operations
for n in range(nt + 1):
    un = u.copy()
    u[1:-1, 1:-1] = un[1:-1, 1:-1] + nu * (dt / dx**2 * (un[2:, 1: -1] - 2 * un[1: -1, 1: -1] + un[0:-2, 1:-1])) \
        + nu * (dt / dy**2 * (un[1:-1, 2:] - 2 * un[1:-1, 1:-1] + un[1:-1, 0:-2]))
    u[0, :] = 1
    u[-1, :] = 1
    u[:, 0] = 1
    u[:, -1] = 1

fig = plt.figure(figsize=(11, 7), dpi=100)
ax = fig.add_subplot(projection='3d')
# surf2 = ax.plot_surface(X, Y, u[:], cmap=cm.viridis)
surf = ax.plot_surface(X, Y, u[:], rstride=1, cstride=1, cmap=cm.viridis, linewidth=0, antialiased=True)
ax.set_zlim(1, 2.5)
ax.set_xlabel('$x$')
ax.set_ylabel('$y$')
plt.show()
