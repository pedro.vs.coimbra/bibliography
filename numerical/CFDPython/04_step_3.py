# Step 3

import numpy  # numpy is a library for array operations akin to MATLAB
from matplotlib import pyplot  # matplotlib is 2D plotting library


def linearconv(nx):
    dx = 2 / (nx - 1)
    nt = 20  # nt is the number of timesteps we want to calculate
    nu = 0.3
    sigma = 0.2
    dt = sigma * dx**2/nu
    dt = 1e-6

    u = numpy.ones(
        nx
    )  # defining a numpy array which is nx elements long with every value equal to 1.
    u[
        int(0.5 / dx):int(1 / dx + 1)
    ] = 2  # setting u = 2 between 0.5 and 1 as per our I.C.s

    un = numpy.ones(
        nx
    )  # initializing our placeholder array, un, to hold the values we calculate for the n+1 timestep

    for _ in range(nt):  # iterate through time
        un = u.copy()  # copy the existing values of u into un
        for i in range(1, nx-1):
            u[i] = un[i] - nu * dt / dx**2 * (un[i+1] - 2 * un[i] + un[i - 1])

    pyplot.plot(numpy.linspace(0, 2, nx), u)
    pyplot.show()


linearconv(40)
