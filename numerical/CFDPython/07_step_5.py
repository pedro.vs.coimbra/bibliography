# Step 1
# 2D Linear convection


from mpl_toolkits.mplot3d import Axes3D    # New Library required for projected 3d plots
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm

nx = 20
ny = 20
nt = 50
dt = 0.01
c = 1
dx = 2 / (nx - 1)
dy = 2 / (ny - 1)

x = np.linspace(0, 2, nx)
y = np.linspace(0, 2, ny)
u = np.ones((nx, ny))

u[int(.5 / dy):int(1 / dy + 1), int(.5 / dx):int(1 / dx + 1)] = 2
# not all this:
# for i in range(nx):
#     for j in range(ny):
#         if 0.5 <= x[i] <= 1 and 0.5 <= y[i] <= 1:
#             u[i][j] = 2
#         else:
#             u[i][j] = 1

# Ployt IC and BC
# fig = plt.figure(figsize=(11, 7), dpi=100)
# ax = fig.add_subplot(projection='3d')
X, Y = np.meshgrid(x, y)
# surf = ax.plot_surface(X, Y, u[:], cmap=cm.viridis)
# plt.show()

# # Solution using nested loops
# un = np.ones((nx, ny))

# for it in range(nt):
#     un = u.copy()
#     for i in range(2, nx-1):
#         for j in range(2, ny-1):
#             u[i][j] = un[i][j] - c * dt / dx * (un[i][j] - un[i - 1][j]) - c * dt / dy * (un[i][j] - un[i][j-1])
#             u[0, :] = 1
#             u[-1, :] = 1
#             u[:, 0] = 1
#             u[:, -1] = 1

# fig = plt.figure(figsize=(11, 7), dpi=100)
# ax = fig.add_subplot(projection='3d')
# surf2 = ax.plot_surface(X, Y, u[:], cmap=cm.viridis)
# plt.show()

# Solution using array operations
for n in range(nt + 1):
    un = u.copy()
    u[1:, 1:] = (un[1:, 1:] - (c * dt / dx * (un[1:, 1:] - un[1:, :-1]))) - (c * dt / dy * (un[1:, 1:] - un[:-1, 1:]))
    u[0, :] = 1
    u[-1, :] = 1
    u[:, 0] = 1
    u[:, -1] = 1

fig = plt.figure(figsize=(11, 7), dpi=100)
ax = fig.add_subplot(projection='3d')
surf2 = ax.plot_surface(X, Y, u[:], cmap=cm.viridis)
plt.show()
