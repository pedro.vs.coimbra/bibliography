#!/bin/bash

# 1. Dado um nome (tipo Décio Machado Nogaio), faça uma Expressão Regular que case somente o primeiro nome (Décio);
grep -Eo '^[^ ]+' <<< "Décio Machado Nogaio"

# 2. Dado um nome (tipo Frei Ada Brusque), faça uma Expressão Regular que case somente o último nome (Brusque);
grep -Eo '\b[[:alpha:]]+$' <<< "Frei Ada Brusque"
## Ver como tirar o espaço que foi a mais. Retirando utilizando a classe posix brusque.

# 3. Verificar uma data, para ver se o dia está entre 01 e 31, o mês entre 01 e 12 e ano entre 2000 e 2999;
## Pegar o dia
grep -Eo '^0?([1-9]|2[0-9]|3[01])/' <<< "30/"
## Pegar dia + mês
grep -Eo '^0?([1-9]|2[0-9]|3[01])/([01][0-2]|0?[0-9])/' <<< "01/05/"
## Pegar data completa
grep -Eo '^0?([1-9]|2[0-9]|3[01])/(0[0-9]|1[0-2])/2[0-9]{3}' <<< "05/05/2022"

# 4. Como o anterior, mas com o dia e o mês podendo ser precedidos ou não por um zero, isto é, um dia pode ser 02 ou simplesmente 2 e mês idem;
grep -Eo '^0?([1-9]|2[0-9]|3[01])/(0?[0-9]|1[0-2])/2[0-9]{3}' <<< "05/5/2022"

# 5. Verificar se um horário está compreendido entre 00:00 e 23:59;
## Primeiro digito
grep -Eo '[0-2][0-3]' <<< "14"
## Correto
grep -Eo '[01][0-9]|2[0-3]' <<< "14"
## Completo
grep -Eo '[01][0-9]|2[0-3]:[0-5][0-9]' <<< "23:59"

# 6. Verificar se um valor monetário foi informado de forma correta, com vírgula separando decimais e ponto separando milhares;
grep -Eo '([0-9]{1,3},)*[0-9]{3}\.[0-9]{2}' <<< "900.00"
grep -Eo '^[0-9]{1,3}(\.[0-9]{3})*,[0-9]{2}$' <<< "900,00"

# 7. Verificar se um endereço de e-mail atende às especificações a seguir:
  # * USUARIO@DOMINIO[.CATEGORIA][.PAIS]
  # * USUARIO - Só pode ter letras, números, ponto (.), sublinha (_) e traço (-), terminando com uma arroba (@);
  # * DOMINIO - Só pode ter letras minúsculas, números e traço (-) com tamanho mínimo de 2 e máximo de 26 caracteres;
  # * CATEGORIA (opcional) - Aceitas .com, .gov, .org, .net, .edu ou nenhuma (como em ufrj.br);
  # * PAIS (opcional) - Duas letras minúsculas.
## Get username
grep -Eo '^[0-9a-zA-z._-]+@[a-z0-9-]{2,26}\.?(com|gov|org|net|edu)?\.?[a-z]{2}$' <<< 'pedro.coimbra@wikki.com.br'
grep -Eo '^[0-9a-zA-z._-]+@[a-z0-9-]{2,26}\.?(com|gov|org|net|edu)?\.?[a-z]{2}$' <<< 'pedro.vs.coimbra@unesp.br'

# 8. Verificar se um endereço de site atende às especificações, observando as
# mesmas restrições para nome de domínio, categoria e país do exercício
# anterior. Protocolos válidos: http, https e ftp. O protocolo, bem como o www
# são opcionais;
## Pegar protoclos e www
grep -Eo '(^(ht)?f?tps?(://)?)(www\.)?' <<< 'http://www.s2dicas-l.com.br/'
## Pegar dominio
grep -Eo '(^(ht)?f?tps?(://)?)(www\.)?[a-z0-9-]{2,26}[^.]' <<< 'http://www.s2dicas-l.com.br/'
## Pegar categoria
grep -Eo '(^(ht)?f?tps?(://)?)(www\.)?[a-z0-9-]{2,26}\.(com|gov|org|net|edu)' <<< 'http://www.s2dicas-l.com.br/'
## Pegar pais
grep -Eo '(^(ht)?f?tps?(://)?)(www\.)?[a-z0-9-]{2,26}\.(com|gov|org|net|edu)\.[a-z]{2}' <<< 'http://www.s2dicas-l.com.br/'
grep -Eo '(^(ht)?f?tps?(://)?)(www\.)?[a-z0-9-]{2,26}\.(com|gov|org|net|edu)(\.[a-z]{2})?' <<< 'https://www.dwservice.net/'

# 9. Verificar se um endereço IP atende às especificações (basta ver se cada octeto tem de 1 a 3 algarismos, não precisa ver se está entre 0 e 255);.
grep -Eo '([0-9]{1,3}(\.|$)){4}' <<< "177.34.168.195"

# 10. Verificar se um endereço de hardware (mac address) atende às especificações, isto é, tem o formato hh:hh:hh:hh:hh:hh, onde cada h é um número hexadecimal.
grep -Eo '^([[:xdigit:]]{2}(:|$)){6}' <<< "2c:f0:5d:a5:ea:0c"
grep -Eo '^([[:xdigit:]]{2}(:|$)){6}' <<< "2804:14d:8081:2c2d:2479:4653:240d:cea1"
grep -Eo '^([[:xdigit:]]{2}(:|$)){6}' <<< "fe80::d83d:76d7:b4b7:88cf"

# Desafio semana 1
# Bom dia Pedro!

# 1. Fazer uma ER para casar com números do CPF, onde os pontos separadores de milhar podem ser substituídos por espaços em branco. Isto é, podem ter um dos dois formatos a seguir:
grep -Eo '([0-9]{3}[. ]){2}[0-9]{3}-[0-9]{2}' <<< '496.039.290-85'

# 2. Criar uma Expressão Regular para descobrir números de telefones fixos em um arquivo, com as seguintes variações:
grep -Eo '^((\+|00)[0-9]{2})? ?\(?[0-9]{2}\)?[0-9]{4}[.-]?[0-9]{4}\b' <<< '+55 1691181595'
# Percebi que essa regex estava pegando número de celulcar o o 9 extra tbm. Nesse caso faltava adicionar a borado ao final né.
# Lembra de nossa discussão em aula? Sobre como fica para colocar uma borada no início se o número começa com o '+'? Qual fica sendo a resposta definitva sobre?


# 3. Um arquivo tem diversos e-mails de clientes, sendo um por linha. Escrever a menor Expressão Regular possível para extrair somente os nomes de cada um dos usuários sem o restante dos endereços.
grep -Eo '^.[^@]+' <<< 'pedro.vs.coimbra@unesp.br'
grep -Eo '^.[^@]+' <<< 'alice_ada@gmail.com'
grep -Eo '^.[^@]+' <<< 'ivana-pita@ferroviaria.com.br'

# Essa eu vi vc falar em aula, achei sensacional essa construção que eu traduzi como até o @ ou até qualquer character né.

Júlio, a resposta demorou chegar por conto do trabalho. Tá tomando um tempo
absurdo por esses dias. Acabei aceitando um desafio de trabalhar em outro
setor da empresa. Originalmente eu estava só no núcleo que desenvolve o nosso
solver numérico para OpenFOAM e agora estou fazendo estágio no núcleo que
desenvolve a perte web da nossa aplicação. Enfim, pretendo usar os recursos do
curso até saber mais shell que vc hahaha. Então não se preocupe que estarei
aqui mais vezes.
