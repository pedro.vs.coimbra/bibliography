#!/bin/bash

# Sequência par e impar
paste <(seq 1 2 9) <(seq 2 2 10)
# Com cabeçalho
echo -e 'Impar\tPar' | cat - <(paste <(seq 1 2 9) <(seq 2 2 10))

ls * | paste -s -d' \f'
ls * | paste - -
