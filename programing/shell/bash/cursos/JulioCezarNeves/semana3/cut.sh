#!/bin/bash

cat numeros
# Espeficicando um range
cut -c1-5 numeros
## Range com valor inicial omitido
cut -c-6 numeros
## Range com fim omitido
cut -c4- numeros
## Speficif numbers
cut -c1,3,5,7,9 numeros
##  Custom range
cut -c -3,5,8- numeros

# CD stuff
cut -f2 -d: musicas
cut -f2 -d: musicas | cut -f1 -d~
head -1 musicas

# Listar extensão de arquivos
cut -f2 -d. ls.txt
# Listar extensão de arquivos excluindo arquivos sem extensão
cut -sf2 -d. ls.txt
