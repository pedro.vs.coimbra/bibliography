#!/bin/bash

# Trocando caracteres
tr o a <<< "Exemplo bem bobo"


# Comprimir caracteres repetidos em apenas 1.
date | cut -f 4 -d ' '
tr -s " " <<< 'Seg Jun  5 10:12:33 BRT 2017' | cut -f 4 -d ' ' 
