#!/bin/bash

# expr
expr 2+7

expr 2 + 7

expr 7 \* 2

expr 7 / 2

# bc
bc <<< "7*5/3"
bc <<< "scale=2; 7*5/3"
bc <<< 12.345\*3

# Exemplo
# echo <<< $(cut -f3 num)
cat num | tr -s " " > /tmp/num$$; mv /tmp/num$$ num
echo "$(cut -d" " -f3 num)0" | bc
echo "$(cut -d" " -f3 num | tr '\n' +)0"
bc <<< "$(cut -d" " -f3 num | tr '\n' +)0"
# Exemplo melhorado 15 anos dps
bc <<< "$(cut -d' ' -f3 num | paste -sd+)"

# Operador aritimético do bash
echo $((i+=3))  # sem definir o i
var=2; echo $((var+2)) # referenciando var sem o cifrão

# Operador ternário
## COND ? VERDADEIRO : FALSO
## COND	É uma condição numérica;
## VERDADEIRO	Será executado caso COND seja verdadeira;
## FALSO	Será executado caso COND seja falso.

var=50
echo $((var>40 ? var-40 : var+40))
echo $((var<40 ? var-40 : var+40))

var=50
var=$((var>40 ? var-40 : var+40))
echo $var
var=$((var>40 ? var-40 : var+40))
var=$[var>40 ? var-40 : var+40]; echo $var
echo $var

# Trocando de base BASE#NUM
echo $[2#100]
