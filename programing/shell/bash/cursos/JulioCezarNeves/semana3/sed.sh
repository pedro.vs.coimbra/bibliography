#!/bin/bash


# Comando p
seq 2 2 12 | sed '3,5p'
seq 2 2 12 | sed -n '3,5p'
seq 2 2 20 | sed -n '/2/,/6/p'
seq 2 2 20 | sed -n '/^2$/,/^6$/p'
# Comando d
seq 2 2 20 | sed '/2/,/6/d'
seq 2 2 20 | sed '/^2$/,/^6$/d'
# Comando a - append
seq 5 | sed '3a\3,5'
## Adicionar linha dps da última
seq 5 | sed '$a \6'
# Comando i - insert
seq 5 | sed '3i\2,5'
# Comando c - change
seq 5 | sed '3c New line 3'
seq 5 | sed '2,4 c\@@ As linhas 2, 3 e 4 foram substituídas @@'

# Substituição de cadeias (s)
sed 's/Pe/Ana/' <<< Pedro
sed 's/.*/&-&/' <<< quero

## Formatar CNPJ
sed 's/./&./2; s/./&./6; s|.|&/|10; s/./&-/15' <<< 12746508000112

## Passar para maiuculo
sed -r '1!d; s/(.*)/\U\1/' <<< quequeisso

sed 's/\b\([[:alpha:]]\+\)\b/\L\u\1/g' <<< "alice ada
eva dias
eça fadoa bessa
ivana pita
maria berta bessa
paulo botelho carvalho
peter punk"

## Com regex
sed -r 's/\b([[:alpha:]]+)\b/\L\u\1/g' <<< "alice ada
eva dias
eça fadoa bessa
ivana pita
maria berta bessa
paulo botelho carvalho
peter punk"
