#!/bin/bash

# educacao@dicas-l.com.br

# 1. Como posso listar somente o conteúdo do 26º registro do arquivo /etc/passwd?
# Obs: Quero somente o conteúdo, sem nada precedendo o registro.
grep -n '.' /etc/passwd | grep '^26' | grep -Eo '[^26:].+' 
# Tem cara de ser uma das mais ineficientes. Mas é oq pensei.

# 2.  Sabendo que os campos de /etc/passwd são separados por dois pontos e o leiaute de seus 4 primeiros campos é:
grep -E ':([0-9].*):\1:' /etc/passwd | grep -Eo '^[^:]+'
# Essa eu até gostei hahaha. Mas se tem uma melhor. Mostre-a ;)

# 3. Quero mandar para saída somente a 22ª (vigésima segunda) linha após o usuário sync do /etc/passwd.
grep -A22 '^sync' ~/passawd | tail -1
