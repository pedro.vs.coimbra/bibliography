#!/bin/bash

# 1. Se eu fizesse:  cat arq > arq1
# Copiaria o conteúdo de arq para arq1.

# 2.  O que aconteceria se eu fizesse (Cuidado com a casca de banana!): cat arq > arq
# O cat mostraria erro e apagaria o conteúdo de arq.
# C)

# 3.
# A. Mesmo resultado.
# B. arq1 arq2 arq3
# C. arq3 arquivo inexistente.
# D. arq1 arq2 arq3

# 4.
# A, B, C

# 5.
# E

# 6.
# A - 3, 5, 6

# 7. 
# 4 Opção -i	1	Lista tudo, menos as linhas nas quais houve casamento
# 5 Opção -l	2	Só casa com palavras inteiras e completas
# 1 Opção -v	3	Indica o arquivo que contém as Expressões Regulares que serão pesquisadas
# 3 Opção -f	4	A pesquisa ignora caixa alta e caixa baixa
# 6 Opção -o	5	Só mostra o nome dos arquivos nos quais houve casamento
# 2 Opção -w	6	Mostra somente o texto que casou

# 8. 
grep -Eo '\<[0-9]{2}\.[0-9]{3}-[0-9]{3}\>' ender

# 9.
ls -l | grep '^d' | grep -Eo '[^ ]+$'
ls -l | grep '^d' | grep -Eo '\S+$'

# 10.
grep -vf exclui /etc/passwd
