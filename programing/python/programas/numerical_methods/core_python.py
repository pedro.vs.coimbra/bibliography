# my second python script
#  ## Integer
#  b = 2
#  print(b)
#  print(type(b))
#
#  ## Float
#  b = b*2.0
#  print(b)
#  print(type(b))
#
#  ## Strings
#  string1 = 'Press return to exit'
#  string2 = 'the program'
#  string3 = (string1 + ' ' + string2)
#  print(string3)
#  print(string1[0:7])
#
#  ## Split
#  s = '3 9 81'
#  print(s.split())
#
#  ## Tuples
#  rec = ('Smith','John',(6,23,68))
#  lastName,firstName,birthdate = rec
#  print(firstName)
#  birthYear = birthdate[2]
#  print(birthYear)
#  name = rec[1] + ' ' + rec[0]
#  print(name)
#  print(rec[0:2])

#  ## Lists
#  a = [1.0, 2.0, 3.0]
#  a.append(4.0)
#  print(a)
#  a.insert(0,0.0)     # Insere o número 0.0 na posição 0
#  print(a)
#  print(len(a))
#  a[2:4] = [5, 5, 8]
#  print(a)
#  b = a
#  b[0] = 9
#  print(a)
#  print(b)
#  ### To create a independent copy of 'a' use:
#  c = a[:]
#  c[0] = 1
#  print(a)
#  print(c)
#  ### Matrices
#  a = [[1, 2, 3], \
#       [3, 2, 3], \
#       [5, 2, 3]]
#  print(a[1])
#  print(a[2][0])
 
#  ## Arithmetic Operators
#  s = 'Hello '
#  t = 'to you'
#  a = [1, 2, 3]
#  print(3*s)
#  print(3*a)
#  print(a + [4, 5])
#  ## a += b -> a = a + b -> Válido para todas operações
#
#  ## Conditionals
#  def sign_of_a(a):
#      if a < 0.0:
#         sign = 'negative'
#      elif a > 0.0:
#         sign = 'positive'
#      else:
#         sign = 'zero'
#      return sign
#  a = -1.4
#  print('a is ' + sign_of_a(a))
#  b = 1.4
#  print('b is ' + sign_of_a(b))
#
#  ## Loops
#  nMax = 5
#  n = 1
#  a = []
#  while n < nMax:
#      a.append(1.0/n)
#      n += 1
#  print(a)
#  nMax = 5
#  a = []
#  for n in range(1,nMax):
#      a.append(1.0/n)
#  print(a)
#  #  name = eval(input('Type a name: '))
#  list = ['Jack', 'Jill', 'Tim', 'Dave']
#  # Python inpu
#  name = 'Tim'
#  for i in range(len(list)):
#      if list[i] == name:
#          print(name,'is number',i + 1,'on the list')
#          break
#      else:
#          print(name,'is not on the litst')
#  ## continue
#  x = []
#  for i in range(1,100):
#      if i%7 != 0: continue
#      x.append(i)
#  print(x)
#
#  ## Formating output w.df -> w é o comprimento do campo de impressão, d o número de dígitos dps do ponto e f é o floating
#  a = 1234.4567
#  n = 9876
#  print('{:7.2f}'.format(a))
#  print('n = {:08d}'.format(n))
#  print('{:12.4e} {:6d} {:20.10e}'.format(a,n,n))
#

#  ## Reading Data from a File
#  data = open('core','r')
#  x = data.readlines()
#  #  print(x)
#
#  f = open('testfile','w+')
#  for k in range(101,111):
#      f.write('{:4d} {:6d}'.format(k,k**2))
#      f.write('\n')
#  f.close()
#
#

# Functions and Modules
## Functions

