perguntas = { 
        'Pergunta 1': {
            'pergunta': 'Quanto é 2+2? ',
            'resposta': { 'a' : '1', 'b' : '4', 'c' : '5', },
            'resposta_certa': 'b',
                      },
        'Pergunta 2': {
            'pergunta': 'Quanto é 3*2? ',
            'resposta': { 'a' : '1', 'b' : '4', 'c' : '6', },
            'resposta_certa': 'c',
                      },
            }

respostas_certas = 0
for pk, pv in perguntas.items():
    print(f'{pk}: {pv["pergunta"]}')

    print('Escolha a alternativa correta:')
    for rk, rv in pv['resposta'].items():
        print(f'[{rk}]: {rv}')

    resp_user = input('Sua resposta: ')

    if  resp_user == pv['resposta_certa']:
        print('Resposta correta')
        respostas_certas += 1
    else:
        print('Resposta errada')
    print(resp_user)
qtd_perguntas = len(perguntas)
porcentagem_acerto = respostas_certas / qtd_perguntas * 100
print(f'Sua nota é {porcentagem_acerto}')
