"""
Dicionários em Python
"""

d1 = {'chave1':'valor da chave'}
d1['nova_chave'] = 'Valor da nova chave'

print(d1['chave1'])
print(d1)

d1 = {
        'str' : 'valor',
        123: 'Outro valor',
        (12,3,4) : 'Doidera',
     }

print(d1[(12,3,4)])

print('str' in d1)
print('str' in d1.keys())
print('valor' in d1.values())


for k in d1.items():
    print(k)
    print(k[0], k[1])

for k, v in d1.items():
    print(k, v)

v = d1.copy()  # O sinal de igual não cria uma cópia.

import copy
v = copy.deepcopy(d1)  # Cópia profunda.
