"""
Reduce
"""

from dados import produtos, pessoas, lista
from functools import reduce

soma_lista = reduce(lambda ac, i: i + ac, lista, 0)

acumula = 0
for item in lista:
    acumula += item

print(acumula)
