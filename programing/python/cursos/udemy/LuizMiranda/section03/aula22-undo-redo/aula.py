"""
Desafio
"""

#  def add_task(task):
#      tasks.append(task)
#      tasks_full_tree.append(task)

def show_op(tasks):
    print(f'\n Tarefas: \n {tasks} \n')

def add(todo, tasks):
    tasks.append(todo)

def undo(todo, redo_list):
    if not tasks:
        print('Nada a desfazer')
        return

    last_task = tasks.pop()
    redo_list.append(last_task)

def redo(todo, redo_list):
    if not redo_list:
        print('Nada a refazer')
        return

    last_redo = redo_list.pop()
    tasks.append(last_redo)

if __name__ == '__main__':
    tasks = []
    redo_list = []

    while True:
        todo = input('Digite uma tarefa ou undo, redo,  ls: ')

        if todo == 'ls':
            show_op(tasks)
            continue
        elif todo == 'undo':
            undo(tasks, redo_list)
            continue
        elif todo == 'redo':
            redo(tasks, redo_list)
            continue

        add(todo, tasks)


#  def add_undo_redo(arg, *task):
#      if arg == 'add':
#          redo.append(tasks)
#          tasks.append(task)
#          #  tasks_full_tree.append(task)
#          return tasks
#      if arg == 'undo':
#          #  tasks_popped_tree = task[-1]
#          #  tasks.pop()
#          tasks_dropped = redo[-1]
#          tasks = redo[-1]
#          return tasks
#      if arg == 'redo':
#          tasks.append(tasks_full_tree[-1])
#          return tasks
#
#  print(add_undo_redo('add', 'Buy clothes'))
#  print(add_undo_redo('add', 'Buy clothes'))
#  print(add_undo_redo('undo', 'Buy clothes'))
#  print(add_undo_redo('redo', 'Buy clothes'))
#
#  #  def redo():
#  #      print('redo')


