"""
Groupby
"""

from itertools import groupby

alunos = [
        {'nome': 'Luiz', 'nota': 'A'},
        {'nome': 'Muiz', 'nota': 'A'},
        {'nome': 'Nuiz', 'nota': 'B'},
        {'nome': 'Ouiz', 'nota': 'C'},
        {'nome': 'Puiz', 'nota': 'A'},
        {'nome': 'Quiz', 'nota': 'C'},
        {'nome': 'Ruiz', 'nota': 'D'},
        {'nome': 'Suiz', 'nota': 'A'},
        {'nome': 'Tuiz', 'nota': 'E'},
        {'nome': 'Uuiz', 'nota': 'F'},
        {'nome': 'Vuiz', 'nota': 'D'},
]

alunos.sort(key=lambda x: x['nota'])
ordena = lambda item: item['nota']
alunos.sort(key=ordena)
alunos_agrupados = groupby(alunos, ordena)

for agrupado, valores_agrupados in alunos_agrupados:
    print(f'Agrupamento: {agrupado}')
    for aluno in valores_agrupados:
        print(aluno)
