"""
04.252.011/0001-10 40.688.134/0001-61 71.506.168/0001-11 12.544.992/0001-05

0   4   2   5   2   0   1   1   0   0   0   1
5   4   3   2   9   8   7   6   5   4   3   2
0   16  6   10  18  0   7   6   0   0   0   2 = 65
Fórmula -> 11 - (65 % 11) = 1
Primeiro digito = 1 (Se o digito for maior que 9, ele se torna 0)

0   4   2   5   2   0   1   1   0   0   0   1   1   X
6   5   4   3   2   9   8   7   6   5   4   3   2
0   20  8   15  4   0   8   7   0   0   0   3   2 = 67
Fórmula -> 11 - (67 % 11) = 10 (Como o resultado é maior que 9, então é 0)
Segundo digito = 0

Novo CNPJ + Digitos = 04.252.011/0001-10
CNPJ Original =       04.252.011/0001-10
Válido

Recap.
543298765432 -> Primeiro digito
6543298765432 -> Segundo digito
"""

import re

def clean_cnpj(cnpj):
    return re.sub(r'[^0-9]', '', cnpj)

def comp_9(digito):
    if (11 - (digito % 11 )) > 9:
        digito = 0
    else:
        digito =  11 - (digito % 11 )
    return(digito)


def primeiro_digito(cnpj):
    cnpj_clean = clean_cnpj(cnpj)
    multipliers = list(range(5,1,-1)) + list(range(9,1,-1))
    soma = sum([multipliers[v] * int(cnpj_clean[v])  for v in range(0,12)])
    digito1 = comp_9(soma) 
    return digito1


def segundo_digito(cnpj):
    cnpj_clean = clean_cnpj(cnpj)
    multipliers = list(range(6,1,-1)) + list(range(9,1,-1))
    soma = sum([multipliers[v] * int(cnpj_clean[v])  for v in range(0,13)])
    digito2 = comp_9(soma) 
    return digito2

def valida(cnpj_original):
    cnpj_clean = clean_cnpj(cnpj_original)
    cnpj_novo = str(cnpj_clean[0:12])+str(primeiro_digito(cnpj_clean))+str(segundo_digito(cnpj_clean))
    if cnpj_novo == cnpj_clean:
        return 'CNPJ válido'
    else:
        return 'CNPJ inválido'
