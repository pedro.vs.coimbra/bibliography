"""
Decoradores
"""

from time import time
from time import sleep

def master(funcao):
    def slave():
        print('Agora estou decorada')
        funcao()
    return slave


@master
def fala_oi():
    print('Oi')

#  fala_oi = master(fala_oi)
fala_oi()

def velocidade(funcao):
    def interna(*args, **kwargs):
        start_time = time()
        resultado =  funcao(*args, **kwargs)
        end_time = time()
        tempo = (end_time - start_time)
        print(f'A função {funcao.__name__} levou {tempo} ms para executar.')
        return resultado
    return interna

@velocidade
def demora():
    for i in range(5):
        print(i)
        sleep(1)
demora()

