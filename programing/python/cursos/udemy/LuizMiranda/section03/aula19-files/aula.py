"""
files
"""

#  file = open('abc.txt', 'a+')
#  file.write('Linha 1\n')
#  file.write('Linha 2\n')
#  file.write('Linha 3\n')
#
#  file.seek(0, 0)
#  print(file.read())
#
#  file.seek(0, 0)
#  print(file.readline(), end='')
#  print(file.readline(), end='')
#  print(file.readline(), end='')
#
#  file.close()

# Jeito pythonic
with open('abc.txt', 'w+') as file:
    file.write('Linha 1\n')
    file.write('Linha 2\n')
    file.write('Linha 3\n')

    file.seek(0)
    print(file.read())
