"""
Map
"""

from dados import lista, pessoas, produtos 

nova_lista = map(lambda x: x * 2, lista)
#  print(lista)
#  print(list(nova_lista))

pecos = map(lambda p: p['preco'], produtos)

#  print(list(pecos))

def aumenta_preco(p):
    p['preco'] = round(p['preco'] * 1.05, 2)
    return p

novos_produtos = map(aumenta_preco, produtos)

#  for produto in novos_produtos:
#      print(produto)

nomes = map(lambda x: x['nome'], pessoas)
for pessoa in nomes:
    print(pessoa)
