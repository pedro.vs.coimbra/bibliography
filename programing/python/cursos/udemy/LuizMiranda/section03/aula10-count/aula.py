"""
count - Itertools
"""

from itertools import count

indice = count()
cidades = ['São Paulo', 'Belo Horizonte', 'Salvador', 'Monte Belo', 'Outra']
estados = ['SP', 'MG', 'BA']

#  cidades_estados = zip(cidades, estados)
cidades_estados = zip(indice, cidades, estados)

for v in cidades_estados:
    print(v)


