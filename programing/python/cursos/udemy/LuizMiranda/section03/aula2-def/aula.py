"""
Funções lambda
"""

def funcao(arg, arg2):
    return arg * arg2
var = funcao(2,2)
print(var)

a = lambda x, y: x * y
print(a(2,2))


lista = [
        ['P2', 14],
        ['P5', 22],
        ['P3', 16],
        ['P4', 19],
        ['P1', 13],
        ]

#  lista.sort(key=lambda item: item[1], reverse=True)
#  lista.sort(key=lambda item: item[0])

print(lista)
