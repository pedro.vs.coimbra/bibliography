"""
Parâmetros mutáveis em funções
"""

def lista_de_clientes(clientes_iterval, lista=[]):
    lista.extend(clientes_iterval)
    return lista

clientes1 = lista_de_clientes(['João', 'Maria', 'Eduardo'])
clientes2 = lista_de_clientes(['Marcos', 'Jonas', 'Zico'])

print(clientes1)
print(clientes2)

