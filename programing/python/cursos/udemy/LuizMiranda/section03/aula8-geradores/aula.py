"""
Geradores em Python
"""
import time
import sys

def gera():
    for n in range(100):
        yield n
        time.sleep(0.1)

#  g = gera()

#  print(next(g))


def gera2():
    variavel = 'Valor 1'
    yield variavel
    variavel = 'Valor 2'
    yield variavel
    variavel = 'Valor 3'
    yield variavel

g = gera2()
#  print(next(g))
#  print(next(g))
#  print(next(g))

# Correct way

for v in g:
    print(v)

l1 = [x for x in range(1000)]
l2 = (x for x in range(1000))

print(sys.getsizeof(l1), sys.getsizeof(l2))


# Os iterados consomem os valores.
