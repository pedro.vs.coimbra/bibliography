"""
Tuplas em Python
"""

t1 = 1, 2, 3, 4
print(type(t1))
t2 = 6, 7, 8, 9
t3 = t1 + t2
n1, n2, n3, *n = t1
t1 = list(t1)
print(type(t1))
