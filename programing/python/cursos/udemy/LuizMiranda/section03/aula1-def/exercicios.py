"""
1
"""

def saudacao(msg, nome):
    print(msg, nome)

saudacao(msg='Olá', nome='Ana Vic')

def soma3(n1, n2, n3):
    print(n1 + n2 + n3)

soma3(1,2,3)

def percent(num, per):
    return num +  num * (per/100)

print(percent(10, 10))

def fizzBuzz(num):
    if num % 3 == 0 and num % 5 == 0:
        return 'FizzBuzz'
    if num % 3 == 0:
        return 'Fizz'
    if num % 5 == 0:
        return 'Buzz'
    return num 

#  print(fizzBuzz(a))

def mestre(funcao, *args, **kwargs):
    return funcao(*args, **kwargs)

def say_hi(name):
    return f'Hi {name}'

def greattings(name, greattings):
    return f'{greattings} {name}'

exec = mestre(say_hi,'Pedro')
print(exec)
exec2 = mestre(greattings, 'Pedro',  greattings='Boa tarde!')
print(exec2)
