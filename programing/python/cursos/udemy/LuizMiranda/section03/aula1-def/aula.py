"""
Funções - def em Python
"""

def saudacao(msg='Olá', nome='user'):
    print(msg, nome)

def f(var):
    print(var)

def dumb():
    return f

#  var = dumb()('Olá')

def func(*args, **keyargs):
    """
    Hello argsons
    """
    print(args)
    print(args[0])
    print(args[-1])
    print(len(args))

func(0,1,2,3,4,5)
lista = [1,2,3,4,5]
print(lista)  # Mostrar a lista desempacotada


