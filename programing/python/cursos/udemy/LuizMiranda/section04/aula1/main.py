from pessoa import Pessoa

p1 = Pessoa('Pedro', 23)
p2 = Pessoa('Ana', 19)

# Esse nome não se aplica a p2
# Esse é um atriduto da classe
#  p1.nome = 'Pedro'

#  print(p1.nome)

#  p1.comer('Maçã')
#  p2.comer('Maçã')
#  p2.parar_comer()
#  p2.comer('Maçã')
#  p2.parar_comer()
p2.falar('POO')

print(Pessoa.gera_id())
print(p1.gera_id())
print(p1.nome)

p3 = Pessoa.por_ano_nascimento('Zé', 1999)
print(p3.idade)
