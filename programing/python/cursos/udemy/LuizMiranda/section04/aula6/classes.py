class Cliente:
    def __init__(self, nome, idade):
        self.nome = nome
        self.idade = idade
        self.enderecos = []

    def insere_enderecos(self, cidade, estado):
        self.enderecos.append(Enderecos(cidade, estado))

    def lista_enderecos(self):
        for enderecos in self.enderecos:
            print(enderecos.cidade, enderecos.estado)

    def __del__(self):
        print(f'{self.nome} Foi apagado')

class Enderecos:
    def __init__(self, cidade, estado):
        self.cidade = cidade
        self.estado = estado

    def __del__(self):
        print(f'{self.cidade}/{self.estado} Foi apagado')
