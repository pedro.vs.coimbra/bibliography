"""
Formatando valores com modificadores
:s - testo (strings)
:d - inteiros (int)
:f - Números de ponto flutunate (float)
:. (NÚMERO)f - Quantidade de casas decimais (flaot)
:(CARACTERE) (> ou < ou ^) (QUANTIDADE)(TIPO - s, d ou f)
"""

num1 = 10
num2 = 3
divisao = num1/num2
#  print(":.2f".format())
print(f"{num1:>5}")
