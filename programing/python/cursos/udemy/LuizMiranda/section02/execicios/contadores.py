"""
for / while
0 10
1 9
2 8
3 7
4 6
5 5
6 4
7 3
8 2
"""

#  b = 0
#  for i in range(10,1,-1):
#      print(b,i)
#      b += 1

#  while b <= 8:
#
#      b += 1

for p, r in enumerate(range(10,1,-1)):
    print(p,r)
