"""
* Criar variáveis para nome (str), idade (int),
* Altura (float) peso (float) de uma pessoa
* Criar variável com ano atual (int)
* Obter o ano de nascimento da pessoa (baseado na idade e no ano atual)
* Obter o IMC da pessoa com 2 casas decimais (peso e na altura da pessoa)
* Exibir um texto com todos os valores na tela usando F-Strings (com chaves)
"""

nome = 'Pedro Vinícius'
idade = 23
altura = 1.84
ano_atual = 2022
ano_nascimento = ano_atual - idade
massa = 105
IMC = massa/altura**2


print(f'{nome} tem {idade} anos, {altura} e pesa {massa} kg.')
print(f'O IMC  de {nome} é {IMC:.2f}')
print(f'{nome} nasceu em {ano_nascimento}')
#  print('{} tem {} anos é'.format(nome, idade))
