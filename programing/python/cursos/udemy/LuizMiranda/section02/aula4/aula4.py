"""
Tipos de dados primitivos
str- string - textos
int - inteiro
float - real/ponto flutuante
bool - booleano/lógico
"""

# Verificando uma classe de variável
print('Luiz', type('Luiz'))
print(10, type(10))
print(25.23, type(25.23))
print(10 == 10, type(10 == 10))

# Converter tipo
print('Luiz', type('luiz'), bool('luiz'))


