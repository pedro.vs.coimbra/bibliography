"""
Split, Join, Enumrate em Python
* Split - Dividir uma string # str
* Join - Juntar uma lista # str
* Enumrate - Enumerar elementos da lista # iteráveis
"""

#  string = "O Brasil é o país do futebol, o Brasil é penta."
#  lista1 = string.split(" ")
#  print(lista1)
#
#  for valor in lista1:
#      qtd_vezes = lista1.strip()

lista = [
        ['Edu', 'João', 'Luiz'],
        ['Edruim', 'ana', 'rei'],
        ['jhon', 'darc', 'del'],
        ]

enumerate = enumerate(lista)

print(enumerate)
