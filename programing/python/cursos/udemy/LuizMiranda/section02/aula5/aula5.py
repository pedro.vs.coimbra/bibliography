"""
Operadores Aritméticos
+,-,*,/,//,** ,%,()
"""

# Operações aritméticas básicas
print('Multiplicação * ', 10 * 10)
print('Adição + ', 10 + 10)
print('Subtração - ', 10 - 10)
print('Divisão / ', 10 / 10)
print('Divisão inteira // ', 10 // 3)
print('Resto da divisão % ', 10 % 5)


# Operador de repetição
print('Repetição * ', 10 * '10')
# Concatenação
print('Concatenação + ', '10' + '10')


