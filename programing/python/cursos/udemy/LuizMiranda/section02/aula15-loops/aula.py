"""
while em Python
words break and continue
"""

#  x = 0
#
#  while x <= 100:
#      print(x)
#      x += 1

"""
While/Else - Aula 8

"""

contador = 0

#  while contador <= 100:
#      print(contador)
#      contador += 1

#       Índicies
#       0123456789................33
#  frase = 'o rato roeu a roupa do rei de roma'
#  tamanho_frase = len(frase)
#
#  while contador < tamanho_frase:
#      print(frase[contador], contador)
#      contador += 1

"""
For in em python
Iterando strings com for
Função range(start=0, stop, step=1)
"""

#  texto = 'Python'
#
#  for n, letra in enumerate(texto):
#      print(n, letra)

for numero in range(2,10,2):
    print(numero)
