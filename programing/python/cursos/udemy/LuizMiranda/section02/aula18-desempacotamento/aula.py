"""
Desempacotamento de listas em Python
"""

lista = [ 'pedro', 'joão', 'maria', 1, 2, 3, 4, 5, 6, 7, 8 ]

n1, n2, *n = lista
print(n1)
