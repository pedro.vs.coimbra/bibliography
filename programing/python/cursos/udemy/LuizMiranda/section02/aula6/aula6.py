"""
Iniciar com letra, pode conter número, separar com _, letas minúculas
"""

nome = 'Pedro Vinícius'
idade = 23
altura = 1.84
massa = 105
e_maior = idade > 18

print('Nome:', nome)
print('Idade:', idade)
print('Altura:', altura)
print('É maior:', e_maior)

print(nome, 'tem', idade, 'anos de idade e seu IMC é', massa/altura**2)
