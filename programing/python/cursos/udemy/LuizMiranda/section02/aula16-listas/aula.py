"""
Listas em Python
fatiamento
append, insert, pop, del, clear, extend, +
min, max
range
"""

#  lista = ['Pedro Coimbra']
#  list = ['A', 'B', 'C', 'D', 'E']
#  l1 = [1,2,3]
#  l2 = [4,5,6]
#  l3 = l1 + l2
#  l2.append('b')
#  l2.insert(2,'kiwi')
#  print(l2)
#  l2.pop(0)
#  print(l2)
#  del(l2[1:2])
#  print(l2)
#
#  l3 = range(1,10)

secreto = 'pedro'
digitadas = []

while True:
    letra = input('Digite uma letra: ')

    if len(letra) > 1:
        print('Digite apenas uma letra.')
        continue

    digitadas.append(letra)

    if letra in secreto:
        print(f'Letra "{letra}" existe na palavra secreta.')
    else:
        print(f'Letra "{letra}" não existe na palavra secreta')
        digitadas.pop()

    sectro_temporario = ''
    for letra_screta in secreto:
        if letra_screta in digitadas:
            sectro_temporario += letra_screta
        else:
            sectro_temporario += '*'

    print(sectro_temporario)
    if sectro_temporario == secreto:
        break

