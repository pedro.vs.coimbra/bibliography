import numpy as np
arr = np.array([1, 2, 3])
print (arr)


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# Para _arrays_ com mais de um eixo/dimensão, pode-se usar a notação semelhante ao _matlab_:

# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# ### 4.1.2. Atributos

# Informações do tipo de elementos está contido na `array` basta utilizar a propriedade `dtype`:

# In[ ]:





# Uma vez que o tipo é definido, apenas guardará elementos do mesmo tipo:

# In[ ]:





# Para se alterar o tipo dos elementos, utilize `astype`

# In[ ]:





# Para se conhecer as dimensões da _array_ e a quant. de elementos de uma array, usar a propriedade _shape_ e _size_:

# In[ ]:





# In[ ]:





# Para se alterar a dimensão da `array`, utilize o método `reshape`:

# In[ ]:





# Para transformar uma array N-D em uma 1-D, utiliza-se o método `flatten`:

# In[ ]:





# ### 4.1.3 Criando Arrays

# Para se criar uma array sem uma lista ou tupla existente,pode-se usar a função `zeros`:

# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# ou `ones`:

# In[ ]:





# Para _arrays_ sequenciais, pode-se utilizar diversas funções: 

# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# ### 4.1.4. Visualização e Cópias

# Ao se realizar métodos ou atribuições em `numpy`, cria-se uma __visualização__ da mesma memória, e não uma cópia. Veremos que isso não afetará as Operações Matemáticas / Matriciais, mas é importante para métodos iterativos e criação de _arrays_ a partir de outras _arrays_:

# In[ ]:





# In[ ]:





# Para se criar uma cópia, utilize o método `copy`:

# In[ ]:





# In[ ]:





# ### 4.1.5. Concatenações

# A concatenação de _arrays_ é um procedimento com diversas possibilidades de programação. Há comandos específicos para apensão em colunas ou linhas, assim como comandos mais gerais para matrizes multidimensionais. Para apensão em linhas, usa-se a função `hstack`.

# In[ ]:





# In[ ]:





# Para apensões em colunas, usa-se o comando `vstack` (observe que o comando adicionou um novo eixo, uma vez que ambas as _arrays_ são unidimesionais):

# In[ ]:





# Os comandos funcionam de acordo com a estrutura da `array`:

# In[ ]:





# In[ ]:





# In[ ]:





# Para uma apensão mais generalizada, usa-se a função `concatenate`, que permite uma apensão para _arrays_ multidimensionais:

# In[ ]:





# Existe também a apensão pelo método `r_`, apesar de possuir uma sintaxe diferente. Para se trabalhar  com tal método para  apensões, deve-se utilizar de uma _string_ de inteiro para se objetivamente dizer como quer que o método opere. É uma _string_ que contém sob qual eixo deseja se concatenar e quantos eixos você deseja em sua _array_:

# Operar como `hstack` unidimensionalmente:

# In[ ]:





# Operar como `vstack`:

# In[ ]:





# Operar como `hstack` bidimensionalmente:

# In[ ]:





# Operações multidimensionais:

# In[ ]:





# A grande vantagem de se utilizar o método `r_` é que as _arrays_ não precisam tem o formato final, o que é muito bom se deseja criar _arrays_ de dimensões superiores a partir de _arrays_ de dimensões inferiores sem que precise alterar sua forma, como foi feito usando a função `concatenate`.

# ## 4.1.6. Condições Lógicas Vetorizadas 

# O objeto _array_, além de permitir operações matemáticas vetorizadas, é capaz de realizar avaliações lógicas vetorizadas, assim como uma matriz no MatLab.

# In[ ]:





# Tais condições podem ser utilizadas para encontrar a posição correspondente a um valor em uma _array_, assim como uma tomada de decisão.

# In[ ]:





# In[ ]:





# ## 4.2. Operações Matemáticas

# <img src="http://www.astroml.org/_images/fig_broadcast_visual_1.png">
# 
# ([image source](http://www.astroml.org/book_figures/appendix/fig_broadcast_visual.html))

# ### 4.2.1. Operações com Escalares:

# Diferente do _matlab_, operações em _arrays_ __não são matriciais__, e sim _element-wise_(elemento por elemento), não necessitando de um ponto antes da operação:

# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# ### 4.2.2. Operações com _arrays_ de mesmo _shape_:

# In[ ]:





# Adição:

# In[ ]:





# Subtração:

# In[ ]:





# Multiplicação (Note que a multiplicação é elemento por elemento, e não matricial):

# In[ ]:





# Divisão:

# In[ ]:





# In[ ]:





# Quando se realiza uma operação e a armazena, cria-se uma visualização da __operação__ sobre a _array_, não sendo da mesma memória:

# In[ ]:





# ### 4.2.3. Operações com _arrays_ de diferentes _shapes_:

# As operações em `numpy` podem ser realizadas com _arrays_ de diferentes shapes, contanto que pelo menos uma das dimensões/eixos possuam o mesmo tamanho.

# In[ ]:





# In[ ]:





# In[ ]:





# Para se realizar as operações pela outra dimensão, precisa-se manipular o formato da variável __C__., alterando sua _shape_. Para mais informações, acesse https://docs.scipy.org/doc/numpy/user/basics.broadcasting.html.

# In[ ]:





# In[ ]:





# Com essa manipulação, pode-se criar _arrays_ que armazenam produtos de uma _array_ com diferentes escalares de uma maneira muito mais simples:

# In[ ]:





# In[ ]:





# In[ ]:





# ## 4.3. Álgebra Linear e Outras Operações

# O módulo `numpy` possui funções algébricas próprias, mas muitas vezes é necessário utilizar um módulo interior de `numpy` para se realizar determinadas operações, denominado `numpy.linalg`. Abaixo, algumas funções próprias:

# Soma:

# In[ ]:





# Soma para diferentes eixos:

# In[ ]:





# In[ ]:





# Matriz diagonal:

# In[ ]:





# Diagonal de uma matriz:

# In[ ]:





# In[ ]:





# Transposição:

# In[ ]:





# In[ ]:





# Multiplicação de matrizes:

# In[ ]:





# In[ ]:





# In[ ]:





# Matriz identidade:
# 

# In[ ]:





# Agora, usando `numpy.linalg`:

# In[ ]:





# Inversão:

# In[ ]:





# In[ ]:





# Determinante:

# In[ ]:





# Autovalor e Autovetor:

# In[ ]:





# In[ ]:





# Solução de Equações Lineares:

# In[ ]:





# ## 4.4. Importação de Dados de Arquivos Texto

# _Python_ é uma linguagem versátil para importação e manipulação de dados de todos os tipos, mas neste curso, focaremos na importação dados numéricos. Para se importar tais dados, podemos usar a função `genfromtxt`. O delimitador, ou seja, aquele que separa os números, é um espaço (_whitespace_) como padrão.

# In[ ]:





# Importando colunas específicas:

# In[ ]:





# Pulando linhas: 

# In[ ]:





# Para salvar os dados, usar a função `savetxt`. No caso,faremos manipulação dos dados e  os salvaremos com um delimitador diferente:

# In[ ]:





# ## 4.5. Regressão Polinomial

# Para se realizar uma regressão polinomial, basta utilizar a função `polyfit`, muito semelhante ao Matlab.

# In[ ]:





# Para a utilização dos coeficientes, cria-se uma função polinomial a partir da função `poly1d`.

# In[ ]:





# In[ ]:





# ## 4.5. Derivação Numérica

# A derivação numérica é um método numérico com pouco suporte em linguagens de programação, seja pela sua simplicidade ou por sua pouca aplicação em comparação a outros métodos (Resolução de EDOs, Sistemas de Equações Lineares, Não Lineares, Integração Numérica, Regressão, etc.). Entretanto, ela possui um suporte por parte do _Python_, com sua função `gradient`. 

# In[ ]:





# In[ ]:


plt.plot(x,sin)
plt.plot(x,cos)
plt.axhline(0,color='k')
plt.axvline(np.pi,color='k')
plt.axvline(np.pi/2,color='k')
plt.axvline(3*np.pi/2,color='k')
plt.xlim(0,2*np.pi)
plt.ylim(-1,1)


# _Links_ para aprofundamento dos estudos:
# 
# * https://docs.scipy.org/doc/ - Site com documentação de Numpy e Scipy.
# * http://scipy.org/NumPy_for_Matlab_Users - Guia de Numpy para usuários de MatLab.
# 
