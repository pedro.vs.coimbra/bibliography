#!/usr/bin/env python
# coding: utf-8

# ![zebra](images/zebra.jpg)

# # 2.Condições & Estruturas de Controle

# __Conteúdo:__ <br>
# 
# * 2.1. Condições (_if_,_else_ e _elif_) <br>
# * 2.2. Laços Condicionais (_while_) <br>
# * 2.3. Laços Determinísticos (_for_) <br>
# 

# ## 2.1. Condições (_if_,_else_ e _elif_)

# Condições básicas: 
#     
# $>$  : Maior <br>
# $<$  : Menor <br> 
# $>=$ : Maior ou igual <br>
# $<=$ : Menor ou igual <br>
# $==$ : Igual <br>
# $!=$ : Diferente <br>

# In[1]:


a = 7
b = 6
c = 5


# A linguagem _Python_ trabalha com __Indentações__, ou seja, não há necessidade de escrever de "definir" um final ao programa. Abaixo, uma exemplificação do comando `if`.

# ![If](images/if.png)

# In[2]:


if a>b:
    print('abacate')


# Usa-se o comando `else` caso se queria realizar outra ação caso a condição não seja alcançada.

# ![If](images/ifelse.png)

# In[3]:


if a>b:
    print('abacate')
else:
    print('abacaxi')


# O comando `elif` pode ser utilizado para mais opções:

# ![If](images/ifelseelif.png)

# In[4]:


if a>b and b<c:
    print('abacate')
elif a>b and b>c:
    print('alcione')
elif a<b and b<c:
    print('alameda')
else:
    print('abacaxi')


# ## 2.2. Laços Condicionais (_while_)

# Os laços condicionais permitem uma repetição de comandos que terminam até determinada condição:

# ![If](images/while.png)

# In[5]:


n = 10
i = 0
while i <= n:
    print(i,end=' ')
    i = i + 1


# O laço pode ser programado de maneira que sua condição não esteja prescrita, mas dentro do código:

# In[6]:


n = 10

i = 0
while True:
    print(i,end=' ')
    if i >= n:
        break
    i = i + 1


# A programação também pode ser feita com uma negação de uma condição:

# In[7]:


n = 10

i = 0
while not i > n:
    print(i,end=' ')
    i = i + 1


# ## 2.3. Laços Determinísticos (_for_)

# Laços Determinísticos permitem a repetição predeterminada de comandos. Há diversas maneiras de inicias se apresentar um _for_, iniciaremos pela mais geral. Note que ele para no valor que __antecede__ o posto:

# ![If](images/for.png)

# In[8]:


for i in range(10):
    print(i,end=' ')


# O valor do iterador tem que _range_ ser __sempre__ um inteiro, se não, não funciona. Ele possui outros dois argumentos: Início (_start_) e Passo (_step_), que são úteis para a manipulação do _for_:

# In[9]:


for i in range(2,10,3):
    print(i,end=' ')


# In[10]:


for i in range(2,10):
    print(i,end=' ')


# Observe que para iteradores de passo negativo, a iteração para no valor que __procede__ o posto:

# In[11]:


for i in range(10,2,-1):
    print(i,end=' ')


# Uma sintaxe mais simples de se escrever um laço é, sempre que possível, iterar sobre uma sequência já existente:

# In[12]:


A = [0,2,3,1,5,6]

for a in A: 
    print(a)


# em strings:

# In[13]:


S = 'abacate'

for s in S:
    print(s)


# em tuplas:

# In[14]:


T = 0,2,54

for t in T:
    print(t)


# é possível iterar duas sequências simultâneas em um loop de `for`, basta que sejam do mesmo tamanho, usando a função _built-in_ `zip`:

# In[15]:


X = [0,1,2,3]
Y = [4,5,6,7]

for x,y in zip(X,Y):
    print(x+y)
 


# Há uma terceira maneira de se programar laços, uma maneira particular da linguagem _Python_, em outras palavras, mais 'pythônica'. É demoninada lista compreensiva(_list comprehension_), de sintaxe mais limpa e eficiência superior:

# In[16]:


[print(i,end=',') for i in range(10)]


# É muito útil para a criação de uma lista precedente da outra, observe os diferentes programas:

# In[17]:


X = [0,1,2,3]
Y = [4,5,6,7]
Z = []

for x,y in zip(X,Y):
    Z.append(x+y)

Z


# In[18]:


X = [0,1,2,3]
Y = [4,5,6,7]

Z = [x+y for x,y in zip(X,Y)]

Z


# Links para aprofundamento de estudo:
# 
# * https://docs.python.org/3/tutorial/controlflow.html - Documentação para Estruturas de Controle
