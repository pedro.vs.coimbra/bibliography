get_ipython().run_line_magic('matplotlib', 'inline')
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np 
def func(x): return x**2 
X = np.r_[0:11:1]
Y = func(X)
plt.figure()
plt.plot(X,Y,label='função')
plt.xlabel('x')
plt.ylabel('y')
plt.title('Título')
plt.legend();
