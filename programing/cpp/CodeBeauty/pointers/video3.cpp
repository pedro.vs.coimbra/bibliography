// Pointers and arrays
#include <iostream>
using namespace std;

int main() {
  int luckyNumbers[5] = {2, 3, 5, 7, 9};
  cout << luckyNumbers << endl;
  cout << &luckyNumbers[0] << endl;
  cout << &luckyNumbers[2] << endl;
  cout << luckyNumbers[2] << endl; // The [] are used to dereference the pointer and get the value of the variable
  cout << *(luckyNumbers+2) << endl; 
}
