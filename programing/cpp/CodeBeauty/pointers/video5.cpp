// Dynamic arrays
#include <iostream>
using namespace std;

int main() {
  // int myArray[5];
  int size;
  cout << "Size: ";
  cin >> size;
  int *myArray = new int [size];

  for (int i = 0; i < size; i++) {
    cout << "Array[" << i << "] ";
    cin >> myArray[i];
  }

  delete [] myArray;
  myArray = NULL;
}
