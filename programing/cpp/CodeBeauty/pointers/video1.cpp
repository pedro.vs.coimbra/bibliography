// Introduction to C++ pointers
#include <iostream>
using namespace std;

int main() {
  int n = 5;
  cout << n << endl;  // print the variable
  cout << &n << endl; // print the adress of the variable
  int *ptr = &n;      // salvando o ponteiro em uma variável
  cout << ptr << endl;
  // dereference a pointer
  cout << *ptr << endl;
  *ptr = 10;
  cout << *ptr << endl;
  cout << n << endl;
}
