#include <iostream>

using namespace std;

// 1. Primeira coisa para fazer ao criar suas funcões é a a definição do tipo de
// retorno

// function declaration
void function();

int main()
{
  function();
  cout << "Hello from main()\n";
}


// function definition
void function() {
  cout << "Hello from function()" << endl;
}

