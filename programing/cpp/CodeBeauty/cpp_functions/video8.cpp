// Lambda expressions
#include <algorithm>
#include <iostream>
#include <stdio.h>
#include <vector>

using namespace std;

int main() {
  struct {
    void operator()(int x) { std::cout << x << "\n"; }
  } something;

  // - [] capture clause
  // - () parameters
  // - {} function definition
  // [cc](p) { fd };
  int d = 3;
  // [d](int x) {
  //   if (x % d == 0)
  //     std::cout << x << " is even number \n";
  //   else
  //     std::cout << x << " is odd number\n";
  // };
  std::vector<int> v{2, 3, 7, 14, 23};
  // std::for_each(v.begin(), v.end(), something);
  std::for_each(v.begin(), v.end(), [d](int x) {
    if (x % d == 0)
      std::cout << x << " is even number \n";
    else
      std::cout << x << " is odd number\n";
  });
}
