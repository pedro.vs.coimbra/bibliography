// What is function overloading?
#include <iostream>

using namespace std;

int sum(int a, int b);
double sum(double a, double b);
float sum(float a, float b, float c);

int main() {
  cout << sum(1, 2) << endl;
  cout << sum(1.5, 2.5) << endl;
  cout << sum(1.5, 2.5, 1) << endl;
}

int sum(int a, int b) {
  return a + b;
}

double sum(double a, double b) {
  return a + b;
}

float sum(float a, float b, float c) {
  return a + b + c;
}

