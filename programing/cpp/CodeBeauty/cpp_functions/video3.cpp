// Functions return statement
// How to check prime number
#include <iostream>
using namespace std;

bool isPrimeNumber(int number) {
  for (int i = 2; i < number; i++) {
    if (number % i == 0) {
      return false;
    }
  }
  return true;
}

int main() {
  int number;
  cout << "Number: ";
  number = 9;

  bool isPrimeFlag = isPrimeNumber(number);

  // bool isPrimeFlag = true;
  // for (int i = 2; i < number; i++) {
  //   if (number % i == 0) {
  //     isPrimeFlag = false;
  //     break;
  //   }
  // }

  if (isPrimeFlag)
    cout << "Prime number" << endl;
  else
    cout << "Not prime number" << endl;
}
