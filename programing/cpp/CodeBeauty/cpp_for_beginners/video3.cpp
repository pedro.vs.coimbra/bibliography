#include <iostream>
#include<list>

using namespace std;


class YouTubeChannel {
// Encapsulation o principio da encapsulação diz que essas variáveis deveria ser privadas.
// Com isso as variáveis pode ser alteradas por meios de métodos que ficam expostos ao usuário.
private:   // Com isso as variáveis ficam acessíveis por fora mas não podem ser alteradas diretamente sem usar um método. #não sei se faz sentido, deve 
  string Name;
  string OwnerName;
  int SubscribersCount;
  list<string> PublishedVideoTitles;


public:
  // Constructor
  YouTubeChannel(string name, string ownerName) {
    Name = name;
    OwnerName = ownerName;
    SubscribersCount = 0;
  }

  // Class methods
  void GetInfo() {
    cout << "Name: " << Name << endl;
    cout << "OwnerName: " << OwnerName << endl;
    cout << "SubscribersCount: " << SubscribersCount << endl;
    cout << "Videos:" << endl;
    for(string videoTitle : PublishedVideoTitles) {
      cout << videoTitle << endl;
    }
  }

  void Subscribe() {
    SubscribersCount++;
  }

  void Unsubscribe() {
    if(SubscribersCount>0)
      SubscribersCount--;
  }

  void PublishVideo(string title) {
    PublishedVideoTitles.push_back(title);
  }

};

int main()
{
  YouTubeChannel ytChannel("CodeBeauty", "Saldina");
  ytChannel.PublishVideo("C++ for beginners");
  ytChannel.PublishVideo("HTML & CSS for beginners");
  ytChannel.PublishVideo("C++ OOP");
  ytChannel.Subscribe();
  ytChannel.Unsubscribe();
  ytChannel.GetInfo();


  YouTubeChannel ytChannel2("AmySings", "Amy");
  ytChannel2.GetInfo();

}
