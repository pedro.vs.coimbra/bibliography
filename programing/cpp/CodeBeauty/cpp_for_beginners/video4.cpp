// Class 4: Inheritance
#include <iostream>
#include<list>

using namespace std;


class YouTubeChannel {
// Encapsulation o principio da encapsulação diz que essas variáveis deveria ser privadas.
// Com isso as variáveis pode ser alteradas por meios de métodos que ficam expostos ao usuário.
private:   // Com isso as variáveis ficam acessíveis por fora mas não podem ser alteradas diretamente sem usar um método. #não sei se faz sentido, deve 
  string Name;
  int SubscribersCount;
  list<string> PublishedVideoTitles;
protected:
  string OwnerName;
public:
  // Constructor
  YouTubeChannel(string name, string ownerName) {
    Name = name;
    OwnerName = ownerName;
    SubscribersCount = 0;
  }

  // Class methods
  void GetInfo() {
    cout << "Name: " << Name << endl;
    cout << "OwnerName: " << OwnerName << endl;
    cout << "SubscribersCount: " << SubscribersCount << endl;
    cout << "Videos:" << endl;
    for(string videoTitle : PublishedVideoTitles) {
      cout << videoTitle << endl;
    }
  }

  void Subscribe() {
    SubscribersCount++;
  }

  void Unsubscribe() {
    if(SubscribersCount>0)
      SubscribersCount--;
  }

  void PublishVideo(string title) {
    PublishedVideoTitles.push_back(title);
  }

};

                         // public isso faz com que os métodos públicos da classe base sejam publicos na classe derivada
class CookingYouTubeChannel:public YouTubeChannel {
public:
  CookingYouTubeChannel(string name, string ownerName):YouTubeChannel(name, ownerName) {

  }
  void Practice() {
    cout << OwnerName << " is practicing cooking, learning new recipes, experimenting with spices..." << endl;
  }

};

int main()
{

  CookingYouTubeChannel cookingYtChannel("Amy's Kitchen", "Amy");
  CookingYouTubeChannel cookingYtChannel2("Pedro's Kitchen", "Pedro");
  cookingYtChannel.PublishVideo("Apple pie");
  cookingYtChannel.PublishVideo("Chocolate cake");
  cookingYtChannel.Subscribe();
  cookingYtChannel.GetInfo();
  cookingYtChannel.Practice();
  cookingYtChannel2.Practice();

  // YouTubeChannel ytChannel("CodeBeauty", "Saldina");
  // ytChannel.
  
}
