#include <iostream>
#include <string>
#include <list>
using namespace std;

struct YoutubeChannel {
  string Name;
  int SubscribersCount;

  YoutubeChannel(string name, int subscribersCount) {
    Name = name;
    SubscribersCount = subscribersCount;
  }

  bool operator==(const YoutubeChannel& channel) const{
    return this->Name == channel.Name;
  }
};  // structure data type
//
// Operator overloading
// Sobrecarrega o operador com nobas operações
ostream& operator<<(ostream& COUT, YoutubeChannel& ytChannel) { // operador & passa os argumentos por referência para não precisar copiá-los
 COUT << "Name: " << ytChannel.Name << endl;
 COUT << "SubscribersCount: " << ytChannel.SubscribersCount << endl;

  return COUT;
}

struct MyCollection {
  list<YoutubeChannel>myChannels;

  void operator+=(YoutubeChannel& channel) {
    this->myChannels.push_back(channel);
  }
  void operator-=(YoutubeChannel& channel) {
    this->myChannels.remove(channel);
  }
};

ostream& operator<<(ostream& COUT, MyCollection& myCollection) {
  for(YoutubeChannel ytChannel : myCollection.myChannels)
    COUT << ytChannel << endl;
  return COUT;
}

int main()
{

  YoutubeChannel yt1 = YoutubeChannel("CodeBeauty", 75000);
  YoutubeChannel yt2 = YoutubeChannel("PedroBeauty", 5000);
  // cout << yt1 << yt2;
  // operator<<(cout, yt1); // só de exemplo o certo é usar normal.
  MyCollection myCollection;
  myCollection += yt1;
  myCollection += yt2;
  myCollection -= yt2;
  cout << myCollection;
  cin.get();
}
