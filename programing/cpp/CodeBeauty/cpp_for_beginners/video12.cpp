// Video 12. Copy constructor
#include <iostream>

using namespace std;

class Book {
public:
  string Title;
  string Author;
  int *Rates;
  int RatesCounter;

  Book(string title, string author) {
    Title = title;
    Author = author;

    RatesCounter = 2;
    Rates = new int[RatesCounter];
    Rates[0] = 4;
    Rates[1] = 5;

    cout << Title + " constructor involked." << endl;
  }

  ~Book() {
    delete[] Rates;
    Rates = nullptr;
    cout << Title + " destrutor involked." << endl;
  }

  // 1. Um construtor de cópia precisa estar na área pública da classe
  // 2. Não tem nenhum tipo de retorno.
  // 3. Tem o mesmo nome da classe que ele pertence.
  // 4. Como parâmetro o constructor de cópia recebe um objeto do tipo Book.
  // Book(Book& original) {      // Um iniciante poderia passar o parâmetro book 
  //   Title = original.Title;   // como valor (book) e não como referência (book&) como é o correto.
  //   Author = original.Author;
  //   Rates = original.Rates;
  //   RatesCounter = original.RatesCounter;
  // } Da forma que está esse código ainda geraria erro no destrutor por este não conseguir
  // desalocar de forma correta a memória.
  
  Book(Book& original) {
    Title = original.Title;
    Author = original.Author;
    Rates = original.Rates;
    RatesCounter = original.RatesCounter;

    Rates = new int[RatesCounter];
    for (int i = 0; i < RatesCounter; i++)
    {
      Rates[i] = original.Rates[i];
    }
  }
};

void PrintBook(Book book) {
  cout << "Title: " << book.Title << endl;
  cout << "Author: " << book.Author << endl;

  float sum = 0;
  for (int i = 0; i < book.RatesCounter; i++)
  {
    sum += book.Rates[i];
  }

  cout << "Avg Rate: " << sum/book.RatesCounter << endl;
}

int main() {
  Book book1("Millionaire Faslane", "M. J. DeMarco");
  Book book2("C++ Lambda Story", "Bartek Filipek");

  Book book3(book1); // Copy constructor.
                     // Há um construtor de cópia padrão, porém para trabalhar
                     // com ponteiros é preciso criar um novo.
  // Book book4=book1; // copy constructor tbm
  // book3 = book2; // Assigmnent operator
  PrintBook(book1);
}
