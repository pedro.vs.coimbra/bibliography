// Class 5: Polymorphism
// Polimorfismo descreve a habilidade de um objeto de ter múltiplas formas.
// Dois objetos quer herdam da mesma classe podem ter o mesmo método mas com implementações diferentes
#include <iostream>
#include<list>

using namespace std;


class YouTubeChannel {
// Encapsulation o principio da encapsulação diz que essas variáveis deveria ser privadas.
// Com isso as variáveis pode ser alteradas por meios de métodos que ficam expostos ao usuário.
private:   // Com isso as variáveis ficam acessíveis por fora mas não podem ser alteradas diretamente sem usar um método. #não sei se faz sentido, deve 
  string Name;
  int SubscribersCount;
  list<string> PublishedVideoTitles;
protected:
  string OwnerName;
  int ContentQuality;
public:

  YouTubeChannel(string name, string ownerName) {  // Constructor
    Name = name;
    OwnerName = ownerName;
    SubscribersCount = 0;
    ContentQuality = 0;
  }
  void GetInfo() {
    cout << "Name: " << Name << endl;
    cout << "OwnerName: " << OwnerName << endl;
    cout << "SubscribersCount: " << SubscribersCount << endl;
    cout << "Videos:" << endl;
    for(string videoTitle : PublishedVideoTitles) {
      cout << videoTitle << endl;
    }
  }
  void Subscribe() {
    SubscribersCount++;
  }
  void Unsubscribe() {
    if(SubscribersCount>0)
      SubscribersCount--;
  }
  void PublishVideo(string title) {
    PublishedVideoTitles.push_back(title);
  }
  void CheckAnalytics() {
    if(ContentQuality<5)
      cout << Name << " This channel has bad quality content" << endl;
    else
      cout << Name << " has great content." << endl;
  }
};

                         // public isso faz com que os métodos públicos da classe base sejam publicos na classe derivada
class CookingYouTubeChannel:public YouTubeChannel {
public:
  CookingYouTubeChannel(string name, string ownerName):YouTubeChannel(name, ownerName) {

  }
  void Practice() {
    cout << OwnerName << " is practicing cooking, learning new recipes, experimenting with spices..." << endl;
    ContentQuality++;
  }

};


class SingersYouTubeChannel:public YouTubeChannel {
public:
  SingersYouTubeChannel(string name, string ownerName):YouTubeChannel(name, ownerName) {

  }
  void Practice() {
    cout << OwnerName << " is taking singing classes, learning new songs, learning how to dance..." << endl;
    ContentQuality++;
  }

};

int main()
{

  CookingYouTubeChannel cookingYtChannel("Amy's Kitchen", "Amy");
  CookingYouTubeChannel cookingYtChannel2("Pedro's Kitchen", "Pedro");
  SingersYouTubeChannel singersYtChannel("Pedro's Canta", "Pedro");
  cookingYtChannel.PublishVideo("Apple pie");
  cookingYtChannel.PublishVideo("Chocolate cake");
  cookingYtChannel.Subscribe();
  cookingYtChannel.GetInfo();
  cookingYtChannel.Practice();
  cookingYtChannel2.Practice();
  singersYtChannel.Practice();
  singersYtChannel.Practice();
  singersYtChannel.Practice();
  singersYtChannel.Practice();
  singersYtChannel.Practice();
  singersYtChannel.Practice();

  YouTubeChannel * yt1 = &cookingYtChannel; // Um ponteira da classe base pode apontar para a classe derivada.
  YouTubeChannel * yt2 = &singersYtChannel;

  yt1->CheckAnalytics();
  yt2->CheckAnalytics();
  singersYtChannel.CheckAnalytics(); //why not do it this way?
  
}
