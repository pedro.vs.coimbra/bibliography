#include <iostream>

using namespace std;

class User {
public:
  string FirstName;
  string LastName;
  int Age;
  string Email;

  User(){} // formato do deafult constructor
  // 1. Construtores se parecem bastante com funções mas com a diferença de que não tem tipo
  // de retorno 
  // 2. Deve ter o mesmo nome da classe que ele pertence
  // 3. Construtores deve ficar na área publica da classe
  // 4. Não recebe nenhum parâmetro
};

int main()
{
  User user1;
  user1.FirstName = "saldina";
  user1.LastName = "nurak";
  user1.Age = 27;
  user1.Email = "saldinanurak@mail.com";
}
