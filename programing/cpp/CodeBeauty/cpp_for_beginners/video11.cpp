// Video 11. Aula sobre destrutor
#include <iostream>

using namespace std;

class Book {
public:
  string Title;
  string Author;
  int* Rates;
  int RatesCounter;

  Book(string title, string author) {
    Title = title;
    Author = author;

    RatesCounter = 2;
    Rates = new int [RatesCounter];
    Rates[0] = 4;
    Rates[1] = 5;

    cout << Title + " constructor involked." << endl;
  }

  // Destrutor.
  // 1. Destrutor sempre começa com tilda.
  // 2. O nome do destrutor é o mesmo nome da classe.
  // 3. O destrutor não tem um tipo de retorno.
  // 4. Não recebe nenhum parâmetro.
  // 5. Precisa ser um membro da minha classe.
  // 6. Precisa estar na área pública da classe.
  // 7. Só pode haver um destrutor por classe.
  ~Book() {
    // delete Rates; // Esse código ainda tem memory leak pq foi alocado um vetor. A forma correta seria:
    delete [] Rates; // O Rates é um ponteiro para o endereço de memória do primeiro elemento do vetor
    Rates = nullptr; // Uma boa prática para ponteiros. Em algumas situações não fazer isso pode causar problemas.
    cout << Title + " destrutor involked." << endl;
  }
  // O destrutor é chamdado no final da execução, nesse exemplo da função main.
  // É executado na ordem inversa do construtor.
};

int main()
{
  Book book1("Millionaire Faslane", "M. J. DeMarco");
  Book book2("C++ Lambda Story", "Bartek Filipek");
}
