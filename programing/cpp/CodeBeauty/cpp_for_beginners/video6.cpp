// Class 6: Virtual functions in C++
// Uma função virtual é uma função que é criada na classe base e redefinida na classe derivada.
// A principal função de um função virtual é dar a habilidade de polimorfismo em tempo de execução (runtime)
// Funções virtuais permitem que seja executada a versão mais derivada de uma função.
#include <iostream>

using namespace std;

class Instrument { // A class is abstract it it contains at least one pure virtual function
public:
  virtual void MakeSound() = 0; // Pure virtual function
};

class Accordion:public Instrument {
  void MakeSound() {
    cout << "Accordion playing...\n";
  }
};


class Piano:public Instrument {
  void MakeSound() {
    cout << "Piano playing...\n";
  }
};


int main()
{
  Instrument* i1 = new Accordion();
  // i1->MakeSound();

  Instrument* i2 = new Piano();
  // i2->MakeSound();

  Instrument* instruments[2] = { i1, i2 }; // Polymorphic behavior
  for (int i = 0; i < 2; i++)
    instruments[i]->MakeSound();
  
}
