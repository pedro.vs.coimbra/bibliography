# Cuda Crash Course

## Aula 1

[Link](https://www.youtube.com/watch?v=2NgpYFdsduY&list=PLxNPSjHT5qvtYRVdNN1yDcdSl39uHV_sU)

* Threads
  - Mais baixo nível de granularidade da execução
  - Executa instruções

Threads são compostas de Warps.

* Warps (SIMT!)
  - Mais baixo nível de entidade agendável
  - Executa intruções em _lock-step_
    - Nem toda thread precisa executar todas as instruções.

Nós de fato programamos em Tread blocks.

* Thread Blocks
  - Mais baixo nível de entidade programável.
  - É designado a um único núcleo _shader_.
  - Pode ser 3-D.

* Grids
  - Grupos de tread blocks.
  - Como um problema é mapeado para a GPU
  - Parte dos parâmetros de inicialização da GPU
  - Can also be 3-D

### Documentação do código vectorAdd.cu

 Cabeçalho

 ```cpp
 __global__ void vectorAdd(const int *__restrict a, const int *__restrict b,
                          int *__restrict c, int N) {
 ```

* `__global__`: indica que a função é um kernel, ou seja, um código que será executado em paralelo em várias threads na GPU.
* `const int *__restrict a`: indica um ponteiro para um vetor a de inteiros constante. O modificador __restrict informa ao compilador que esse ponteiro não pode ser usado para acessar outras variáveis além de a.
* `const int *__restrict b`: indica um ponteiro para um vetor b de inteiros constante. O modificador __restrict informa ao compilador que esse ponteiro não pode ser usado para acessar outras variáveis além de b.
* `int *__restrict c`: indica um ponteiro para um vetor c de inteiros que receberá o resultado da operação de soma dos vetores a e b. O modificador __restrict informa ao compilador que esse ponteiro não pode ser usado para acessar outras variáveis além de c.
* `int N`: indica o tamanho dos vetores a, b e c, ou seja, o número de elementos que serão somados.

## Aula 2

Segundo código onde a parte de alocação de memória é tercerizada.

## Aula 3: Multiplicação de matriz

Fluxo base para multipliacação de matrizes: $A \times B = C$

1. Designa uma thread para cada elemento da matriz de resltados $\mathbf{B}$.
2. Cada thread atravessa uma linha da matriz $\mathbf{A}$ e uma coluna da matriz $\mathbf{B}$.
3. Cada thread escreve o resultados para seu elemento designado em $C$

### Indexação para thread blocks


