---
title: Automatic Markdown Website and Résumé/CV with Pandoc **Notes**
subtitle: By Luke Smith
linkcolor: blue
output: pdf_document
---

# What is the content of this video?
In this video Luke Smith shows the similarity between the file of his CV and website, so he teach how to compile the same file using pandoc to a website and to a pdf. Using a template in latex and a style in .css file.

# Notes
pandoc index.md -s -c style.css --toc -o - index.html
pandoc index.md --template=template.tex --pdf-engine=xelatex -o index.pdf

# Video url não é pra ser section
https://youtu.be/HggKRBvjIs
