---
title: An Intro To R Markdown **Notes**
subtitle: By Luke Smith
output: pdf_document
---

# What is the content of this video?
 This video is an introduction to R Markdown, in this video Luke Smith shows some programs that are need to compile .md files. Besides that he taught how to compile these files using a vim macro, he told that is possible to put arbitraty blocks of \LaTeX\ code inside the markdown file and insert code of other and execute them in the output languages using ```{languages_name}. 

# Notes
## Packages
install.packages("reticulate") to run python code

install.packages("rmarkdown")

## Compile using vim
```{vim}
autocmd Filetype rmd inoremap map \c :!echo<space>"require(rmarkdown);<space>render('<c-r>%')"<space>\|<space>R<space>--vanilla<enter>
```
## Inclue bibliography
To do that, add to the heading
bibliography: "bibliography file path"
to cite them @ReferenceName

# New words
<++>


# Video url não é pra ser section
https://www.youtube.com/watch?v=4J5a0JWIF-0
