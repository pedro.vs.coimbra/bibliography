function Xs = BissecaoRaiz(F,a,b,TolMax)
 % Exercicio 14
Fa = F(a); Fb = F(b);
if Fa*Fb>0
  disp('Erro: A funcao tem o mesmo sinal nos pontos a e b.')
else 
  %Calcular o número de iteraçoes comforme o exercicio 3.1
  I = (log(b-a)-log(TolMax))/log(2);
  if (I-floor(I))>0.5
    imax=ceil(I);
  else
    imax = floor(I);
  end
  fprintf(' O numero maximo de iteracoes sera  %3i \n',imax)
  for i = 1:imax
    xNS = (a+b)/2;
    toli = (b-a)/2;
    FxNS = F(xNS);
   if FxNS==0
    Xs = xNS;
   break
   end
   if toli < TolMax
    Xs = xNS;
   break
   end
   if F(a)*FxNS < 0
    b = xNS;
   else
    a = xNS;
   end
  end
 end