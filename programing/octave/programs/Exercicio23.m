% Exercicio 23
clear all
clc
% Definindo a funcao
R = inline('100.*(1 + (3.90802*10^-3).*T - (0.580195*10^-6).*T.^2) ');

% Plotando o grafico de R(T)
T = [-400:200];
plot(T,R(T))
grid on

% Pelo metodo da BissecaoRaiz
fprintf('\n Pelo metodo da BissecaoRaiz\n')
Solution = BissecaoRaiz(R,-260,-230,0.001)

% Pelo fzero
fzero(R,-230)
