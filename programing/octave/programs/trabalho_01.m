clear all
clc

  F = inline('y=2.77778 * sin(0.1 * x).*sin(0.9*x)');
a = 3; b = 5; imax = 30; tol = 0.001;
Fa = F(a); Fb = F(b);
if Fa*Fb>0
  disp('Erro: A funcao tem o mesmo sinal nos pontos a e b.')
else 
  disp('  I        a          b    (xNS)Solucao  f(xNS)   Tolerancia')
  for i = 1:imax
    xNS = (a+b)/2;
    toli = (b - a)/2;
    FxNS = F(xNS);
    fprintf('%3i %11.6f%11.6f%11.6f%11.6f%11.6f\n', i, a, b, xNS, FxNS, toli)
     if FxNS==0
     fprintf(' Uma solucao exata x=%11.6f foi encontrada' , xNS)
     break
     end
     if toli < tol
     break
   end
   if i==imax
    fprintf(' Solucao nao foi obtida em %i iteracoes', imax)
    break
   end
   if F(a)*FxNS < 0
    b = xNS;
   else
    a = xNS;
   end
  end
 end
