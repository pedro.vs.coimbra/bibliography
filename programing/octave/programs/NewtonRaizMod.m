function [Xs,FXs,iact] = NewtonRaizMod(Fun,FunDer,Xest,Err,imax)
  
  % NewtonRoot determina a raiz de Fun = 0 na vizinhaça do ponto Z
  % Variaveis de entrada
  %   Fun funçao que calcula Fun para dado x
  %   FunDer Name of a user-de fined function that calculates the derivative
  %         of Fun for a given x
  % Xest inicial estimation of the solution
  % Err Maximum error
  % imax Maximum number of iterations
  % Output variable
  % Xs Solution
  
for i = 1:imax
    Xi = Xest-feval(Fun,Xest)/feval(FunDer,Xest);
    if abs((Xi-Xest)/Xest) < Err
      Xs = Xi;
      FXs = Fun(Xs);
      iact = i;
      fprintf('Solucao encontrada em %i iteracoes\n',i)
    break
    end
    Xest = Xi;
end
 if i == imax
    fprintf('Solucao nao foi encontrada para %i iteracoes. \n' ,imax)
    Xs = ( 'Sem resposta ' );
end
