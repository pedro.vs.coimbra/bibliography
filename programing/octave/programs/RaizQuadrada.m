function [Xs] = RaizQuadrada(p)
% Exercicio 15 calculo da raiz quadrada
Fun = @(x) x.^2-p;
FunDer = @(x) 2*x;
Err = 0.00001;
imax = 15;
 if p < 0
  Xs = disp(' Erro: Numero negativo, nao possui raiz real');
  else 
  Xest=p;
  for i = 1:imax
    Xi = Xest-feval(Fun,Xest)/feval(FunDer,Xest);
    if abs((Xi-Xest)/Xest) < Err
      Xs = Xi;
      fprintf('Solucao encontrada em %i iteracoes\n',i)
    break
    end
    Xest = Xi;
  end
  if i == imax
    fprintf('Solucao nao foi encontrada para %i iteracoes. \n' ,imax)
    Xs = ( 'Sem resposta ' );
  end
 end