% Ajuste de curvas usando uma funcao nao linear
clear all
clc
texp=2 : 2 : 30 ;
vexp=[9.7 8.1 6.6 5.1 4.4 3.7 2.8 2.4 2.0 1.6 1.4 1.1 0.85 0.69 0.6]; %  Entra com os dados experimentais
vexpLOG=log(vexp); % Calcula ln(y) dos dados (para uso na regressao linear)
R=5E6;                                                                          
[a1,a0]=LinearRegression(texp, vexpLOG)   % Calcula os coeficientes a1 e a0 com a funcao RegressaoLinear do Exemplo 5-1
b=exp(a0)                                                                     % Calcula b, sabendo que a0=ln(b) (ver Tabela 5-2).
C=-1/(R*a1)                                                                 % Calcula C usando Eq. (5.18)
t=0:0.5:30;
v=b*exp(a1*t);                                                              % a1 e m na equacao v = be^m
plot(t,v,texp,vexp,'ro')
xlabel('Tempo(s)', 'fontsize' , 15)
ylabel('vr(V)', 'fontsize' , 15)
grid