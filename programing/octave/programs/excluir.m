
clear
clc
R = inline('100.*(1 + (3.90802e-3).*T - (0.580195e-6).*T.^2)  ');
T = [-400:1:-200];
plot(T, R(T))
grid on;
