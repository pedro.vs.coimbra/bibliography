function y=RegresSub(a,b)
  % A funcao resolve um sitema de equcoes lineares ax=b, onde a e uma 
  % Matriz triangular superior, usando a substitiuiçao regressiva
  % Variaveis de entrada:
  % a Matriz dos coeficientes
  % b Vetor coluna de constantes.
  % Variavel de saida.
  % y Vetor coluna solucao
  
  n=length(b);
  y(n,1)=b(n)/a(n,n);
  for i= n-1:-1:1
    y(i,1)=(b(i)-a(i,i+1:n)*y(i+1:n,1))./a(i,i);               %Eq.(4.5)
  end
  