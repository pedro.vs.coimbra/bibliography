% Exercicio 29
clear all
clc
F = inline ('- 0.15 + 24 / (140^2 + (2*pi*f*0.260 - (1/(2*pi*f*.000025)))^2)^(1/2)');

% (a) Calculado pela funcaca SecanteRaiz
fprintf('\n(a) Calculado pela funcaca SecanteRaiz\n')
Solucao = SecanteRaiz(F,30,50,0.0001,20)

% (b) Calculado pela funcao fzero
fprintf('\n(b) Calculado pela funcao fzero\n')
Solucao = fzero(F,40)
