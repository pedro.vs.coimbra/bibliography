   % Lista de exercicios
   disp('Lista de exercícios')

% Exercício 1
disp('Exercício 1')
disp('a)')

x = [2 5 6 8 9 13 15]
y = [7 8 10 11 12 14 15]
[a1,a0] = LinearRegression(x,y)

disp('b)')
E = ErroGlobal(x,y,a1,a0,7)

% Exercício 2
disp('Exercicio 2')

disp('a)')
x = [-7 -5 -1 0 2 5 6]
y = [15 12 5 2 0 -5 -9]
[a1,a0] = LinearRegression(x,y)

disp('b)')
E = ErroGlobal(x,y,a1,a0,7)

% Exercício 3

