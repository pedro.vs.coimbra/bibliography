function E=ErroGlobal(x,y,a1,a0,n)
% This function evaluate de error assosiated with curve fitting.
for i=1:n
  e(i)=(y(i)-(a1*x(i)+a0)).^2;
end

E=sum(e);
