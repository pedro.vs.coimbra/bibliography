function Yint = LagrangeINT(x,y,Xint)
  %LagrangeINT ajusta um polinomio de Lagrange a um conjunto de pontos dados e usa o polinomio para determinar o valor interpolado de um ponto.
  % Variaveis de entrada:
  % x Vetor com as coordenadas x dos pontos dados.
  % y Vetor com as coordenadas y dos pondtos dados.
  % Xint A coordenada x do ponto a ser interpolado.
  % Variavel de saida:
  % Yint O valor interpolado Xint.
  
  % O comrimento do vetor x fornece o numero de termos do polinomio
  n = length(x); 
  % Calcula os termos Li do produtorio
  for i = 1:n
    L(i)=1;
    for j=1:n
      if j~=i
        L(i)=L(i)*(Xint-x(j))/(x(i)-x(j));
      end
    end
  end
  % Calcula o valor do polinomio Eq.(5.45)
  Yint=sum(y.*L); 