function Xs = BissecaoRaiz2(F,a,b,imax,tol)

Fa = F(a); Fb = F(b);
if Fa*Fb>0
  disp('Erro: A funcao tem o mesmo sinal nos pontos a e b.')
else 
  disp('  I        a          b    (xNS)Solucao  f(xNS)   Tolerancia')
  for i = 1:imax
    xNS = (a+b)/2;
    toli = (b - a)/2;
    FxNS = F(xNS);
    fprintf('%3i %11.6f%11.6f%11.6f%11.6f%11.6f\n', i, a, b, xNS, FxNS, toli)
     if FxNS==0
     fprintf(' Uma solucao exata x=%11.6f foi encontrada \n' , xNS)
     break
     end
     if toli < tol
        fprintf(' A solucao obtida para %3i iteracoes foi %11.6f\n',i,xNS)
     break
   end
   if i==imax
    fprintf(' A solucao obtida para %3i iteracoes foi %11.6f\n',i,xNS)
    break
   end
   if F(a)*FxNS < 0
    b = xNS;
   else
    a = xNS;
   end
  end
 end