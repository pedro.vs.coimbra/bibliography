function x=Gauss(a,b)
  % A funcao resolve um sistema de equacoes lineares [a][x]=[b] usando o metodo de eliminacao de Gauss
  % Variaveis de entrada: 
  % a Matriz de coeficientes
  % b Vetor coluna contendo as constantes do lado dierito do sistema
  % Variavel de saida:
  % x Vetor coluna com a solucao
  
 ab=[a,b];            %Incorpora o vetor coluna [b] à matriz [a].
 [R,C]=size(ab);
for j=1:R-1         % j - representa o numero da linha pivo
  for i=j+1:R       % i - Linha que esta tentando cancelar o coeficientes
  ab(i,j:C)=ab(i,j:C)-ab(i,j)/ab(j,j)*ab(j,j:C);
  end                   %Multiplicador m i j\ ab(j,j), elemento pivo\ ab(j,j:C) equacao pivo
end
x=zeros(R,1);                                                     % Substitituição regressiva a partir daqui
x(R)=ab(R,C)/ab(R,R);
for i=R-1:-1:1
  x(i)=(ab(i,C)-ab(i,i+1:R)*x(i+1:R))/ab(i,i);
end




