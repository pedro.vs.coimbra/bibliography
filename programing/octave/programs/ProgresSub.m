function y=ProgresSub(a,b)
  % A funcao resolve um sistema de equacoes lineares ax=b, onde a e ua matriz triangular inferior, usando a substituicao progressiva.
  % Variaveis de entrada:
  % a Matriz de coeficientes
  % b Vetor coluna de constantes
  % Variavel de saida
  % y Vetor coluna com a solucao
  
  n=length(b);                                                                    
  y(1,1)=b(1)/a(1,1);
  for i=2:n
    y(i,1)=(b(i)-a(i,1:i-1)*y(1:i-1,1))./a(i,i);                      % Eq. (4.8)
  end
  