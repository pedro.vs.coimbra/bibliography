% Exercicio 19, Modificar a funcao NewtonRaiz.

F = inline('x.^2-exp(-x)');
Fder = inline('2*x+exp(-x)');

[a,b,c] = NewtonRaizMod(F,Fder,1,0.01,20)