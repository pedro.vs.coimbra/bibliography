function [L,U]=LUdecompCrout(A)
  % A funcao decompoe a matriz A em uma matriz triangular inferior L e em uma matriz triangular superior U, usando o metodo de Corut de forma que A = LU.
  % Variaveis de entrada:
  % A Matriz de coeficientes.
  % b vetor coluna de constantes
  % Variaveis de saida
  % L Matriz triangular inferior.
  % U Matriz triangular superior.
  
  [R,C]=size(A);
  for i=1:R                                           %
    L(i,1)=A(i,1);  %Eq. (4.34)          % Passos 1 e 2 (p. 136)
    U(i,i)=1;            %Eq. (4.35)          %
  end                                                       %
  for j=2:R
    U(1,j)=A(1,j)/L(1,1);     %Eq.(4.36)                                                %Passo 3(p.136)
  end
  for i=2:R                                                                                                 %
    for j=2:i
      L(i,j)=A(i,j)-L(i,1:j-1)*U(1:j-1,j);               %Eq.(4.37)
    end                                                                                                           % Passo 4 (p.136)
   for j=i+1:R
     U(i,j)=(A(i,j)-L(i,1:i-1)*U(1:i-1,j))/L(i,i);  %Eq.(4.38)
   end
 end                                                                                                              %
 