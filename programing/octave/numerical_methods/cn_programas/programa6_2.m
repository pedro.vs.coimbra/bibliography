texp=2 : 2 : 30 ;
vexp=[9.7 8.1 6.6 5.1 4.4 3.7 2.8 2.4 2.0 1.6 1.4 1.1 0.85 0.69 0.6];
vexpLOG=log(vexp);
R=5E6;
[a1,a0]=LinearRegression(texp, vexpLOG)
b=exp(a0)
C=-1/(R*a1)
t=0:0.5:30;
v=b*exp(a1*t);
plot(t,v,texp,vexp,'ro')
grid