%Exercicio 22
clear all
clc

F = inline('((170000.*x)*((1 + x/12).^240))/12 - 1250*((1 + x/12).^240) + 1250');
Fder = inline('-25000*((1 + x/12)^239) + 14166.666*((1 + x/12)^240) + 28333.3333*((1 + x/12)^239)');

% (a) Calculado pela funcaca NewtonRaiz
fprintf('\n(a) Calculado pela funcaca NewtonRaiz\n')
Solution = NewtonRaiz(F, Fder, 1, 0.001, 15)

% (b) Calculado pela funcao fzero
fprintf('\n(b) Calculado pela funcao fzero\n')
Solucao = fzero(F,1)
