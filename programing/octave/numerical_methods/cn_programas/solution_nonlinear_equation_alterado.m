clear all
clc
F = inline('2-3.8*((x.^3)-cos(x))');
a = -4; b = 4; imax = 20; tol = 0.001;
Fa = F(a); Fb = F(b);
if Fa*Fb>0
  disp('Erro: A funcao tem o mesmo sinal nos pontos a e b.')
else 
  disp('iteraction      a       b   (xNS)    Solucao f(xNS)    Tolerancia')
  for i = 1:imax
    xNS = (a+b)/2;
    toli = (b - a)/2;
    FxNS = F(xNS);
    fprintf(' %11.6f    %11.6f   %11.6f   %11.6f\n', i, a, b, xNS, FxNS, toli)
     if FxNS == 0
     fprintf(' Uma solucao exata x = %11.6f foi encontrada' , xNS)
     break
     end
  if toli < tol
    break
  end
  if i == imax
    fprintf(' Solucao nao foi obtida em %i iteractions', imax)
    break
  end
  if F(a)*FxNS < 0
    b = xNS;
  else
    a = xNS;
  end
 end
 end

 