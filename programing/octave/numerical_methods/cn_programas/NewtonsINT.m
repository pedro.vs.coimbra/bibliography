function Yint = NewtonsINT(x,y,Xint)
         %NewtonsINT ajusta um polinomio de Newton a um dado conjunto de pontos e usa esse polinomio para determinar o valor interpolado de um ponto.
         %Variaveis de entrada:
         % x Vetor com as coordenadas x dos pontos dados.
         % y Vetor com as coordenadas y dos pontos dados.
         % Xint Coordenada x do ponto a ser interpolado.
         % Variavel de saida:
         %Yint: o Valor interpolado de Xint.
  
n = length(x);  %O comporimento do vetor x fornece o número de coeficientes (em termos) do polinômio.
a(1)=y(1); % O primeiro coeficiente a1.
for i=1:n-1
  divDIF(i,1)=(y(i+1)-y(i))/(x(i+1)-x(i)); % Calcula as diferenças dicidias. Elas são armazenadas na primeira coluna de divDIF.
end
for j=2:n-1
  for i=1:n-j
    divDIF(i,j)=(divDIF(i+1,j-1)-divDIF(i,j-1))/(x(j+i)-x(i)); % Calcula as diferenças divididas de odem 2 e superior (até) a ordem (n-q)). Os valores são atribuídos às as colunas de divDIF.
  end
end
for j=2:n
  a(j)=divDIF(1,j-1); % Atribui os coeficientes a2 e an ao vetor a.
end
Yint=a(1);
xn=1;
for k=2:n              % Calcula o valor interpolado de Xint. O primeiro termo do polinômio é a1. Os termos seguintes são adicionados por meio de um loop.
  xn=xn*(Xint-x(k-1));
  Yint=Yint+a(k)*xn;
end
