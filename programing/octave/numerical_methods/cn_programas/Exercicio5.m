%Determine a raiz cubica de 155, usando o metodo de Newton
clear all
clc
f = inline('x.^3-155');
fder= inline('3*x.^2');

Solucao_Newton=NewtonRaiz(f,fder,5,0.01,5)
