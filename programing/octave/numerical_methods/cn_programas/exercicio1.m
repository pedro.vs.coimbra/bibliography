%Exercicio 1 determine a raiz positiva do polinomio.
clear all
clc
  f=inline('x.^3 + 3.8*x.^2-8.6*x-24.4');
  fder=inline('3*x.^2 + 7.6*x-8.6');

%  Plotar e calacular a raiz pelo metodo de Newto  
disp('(a) Plotar a funcao \n')
x = -5:5;
plot(x,f(x))
grid on
Solucao_Newton = NewtonRaiz(f,fder,2,0.01,5)