%Exercicio 7 determinar todas as raizes de um polinomio, utilizando o metodo da bissecao
clear all
clc

f = inline('x.^3+3.8*x.^2-8.6*x-24.4');

%Ploar o grafico da funcao
x = -5:0.1:3;
plot(x,f(x))
grid on
% Calculo da primeira raiz
fprintf('\n Calculo da primeira raiz \n' )
BissecaoRaiz2(f,-5,-4,5,0.01)

%Calculo da segunda raiz
fprintf('\n Calculo da seguda raiz \n' )
BissecaoRaiz2(f,-3,-1,5,0.01)

% Calculo da terceira raiz
fprintf('\n Calculo da terceira raiz \n' )
BissecaoRaiz2(f,2,3,5,0.01)
