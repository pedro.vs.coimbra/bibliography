% Determine a raiz de f(x)=x^2 - exp(-x)
clear all
clc

fprintf('(a) Usando o metodo da bissecao, com a=0,b=1,imax=5:\n')

  F = inline('x.^2-exp(-x)');
  a=0; b=1; imax=5; tol=0.01;
  Fa = F(a); Fb = F(b);
  if Fa*Fb>0
   disp('Erro: A funcao tem o mesmo sinal nos pontos a e b.')
  else 
    disp('  I        a          b    (xNS)Solucao  f(xNS)   Tolerancia')
    for i = 1:imax
      xNS = (a+b)/2;
      toli = (b - a)/2;
      FxNS = F(xNS);
      fprintf('%3i %11.6f%11.6f%11.6f%11.6f%11.6f\n', i, a, b, xNS, FxNS, toli)
      if FxNS==0
        fprintf(' Uma solucao exata x=%11.6f foi encontrada' , xNS)
      break
     end
      if toli < tol
      break
   end
   if i==imax
    fprintf('\n Solucao nao foi obtida em %i iteracoes', imax)
    break
   end
   if F(a)*FxNS < 0
    b = xNS;
   else
    a = xNS;
      end
    end
  end

fprintf('\n\n(b)Usando o metodo da secante, com x1=0,x2=1,imax=5:\n')
  Solucao_Secante = SecanteRaiz(F,0,1,0.01,5)

fprintf('\n\n(c)Usando o metodo de Newton, com x1=0,imax=5;n')
  G = @(x)(2*x+exp(-x));
  Solucao_Newton = NewtonRaiz(F,G,1,0.01,5)

