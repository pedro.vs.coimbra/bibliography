clear, clc

As = 8;
r = 3;
theta = [0:0.1:2]*pi;

f = As - 1/2 * r^2 * ( theta - sin(theta));

plot(theta, f) % Plotar  o Grafico de f

title(' Function As') % Título do Grafico
xlabel('\theta (rad)') % Rotulo do eixo x
