function x=GaussPivot(a,b)
  % A funcao resolve um sistema de equacoes lineares [a][x]=[b] usando o metodo de eliminacao de Gauss
  % Variaveis de entrada: 
  % a Matriz de coeficientes
  % b Vetor coluna contendo as constantes do lado dierito do sistema
  % Variavel de saida:
  % x Vetor coluna com a solucao
  
  ab=[a,b];
  [R,C]=size(ab);
  for j=1:R-1
  % Comeca a secao de pivotacao
      if ab(j,j)==0                                                 %Verifica se o elemento pivô é nulo.
        for k=j+1:R                                               %Se a pivotação for necessária, procura-se uma linha contendo um elemento pivô diferente de zero.
          if ab(k,j)~=0
            abTemp=ab(j,:);                               %
            ab(j,:)=ab(k,:);                                   %Troca a linha que tem o elemento pivô nulo pela linha que tem o elemento pivô não-nulo.
            ab(k,:)=abTemp;                              %
            break %Termina a busca por uma linha com elemento pivô diferente de zero.
          end
        end
      end
    % Termina a secao de pivotacao
    for i=j+1:R
      ab(i,j:C)=ab(i,j:C)-ab(i,j)/ab(j,j)*ab(j,j:C);
    end
  end
  x=zeros(R,1);
  x(R)=ab(R,C)/ab(R,R);
  for i=R-1:-1:1
    x(i)=(ab(i,C)-ab(i,i+1:R)*x(i+1:R))/ab(i,i);
  end
  
    