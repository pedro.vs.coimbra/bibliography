clc
clear
% Script para desenho do Gráfico da Função

x= [0:0.0001:60];
y=2.77778 * sin(0.1 * x).*sin(0.9*x);
plot(x,y);
title('Um batimento')
xlabel('t')
ylabel('s')
grid on