function [a1,a0] = RegressaoLinear(x, y)
  %RegressaoLinear calcula os coeficientes a1 e a0 da equação linear
  % y = a1*x + a0 que melhor se ajusta aos n pontos do conjunto de dados.
  %Variáveis de entrada:
  %x Vetor com as coordenadas x dos pontos.
  %y Vetor com as coordenada y dos pontos.
  %Variáveis de saída:
   %a1 Coeficiente a1.
  %a0 Coeficiente a0.
  
  nx = length(x);
  ny = length(y);
  if nx ~=ny                                                        % Verifica se os vetores x e y tem o mesmo numero de elementos
    disp(' Erro: O numero de elementos em x deve ser o mesmo que em y.')
    a1 = 'Erro';                                                      % Se sim o  MATLAB exibe uma mensagem de erro e as constantes nao sao calculadas
    a0 = 'Erro';
  else
    Sx = sum(x);
    Sy = sum(y);                                                   % Calcula os termos com as somas nas Eqs. (5.13)
    Sxy = sum(x.*y);
    Sxx = sum(x.^2);
    a1 = 