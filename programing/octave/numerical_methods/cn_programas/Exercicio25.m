% Exercicio 25
clear all
clc
Q = inline('pi*0.1*25*(10*(Ts - 298) + 0.8*5.67*10^-8*(Ts^4 - 298^4)) - 18405  ');

% (a) Calculado pela funcaca BissecaoRaiz
fprintf('\n(a) Calculado pela funcaca BissecaoRaiz\n')
Solucao = BissecaoRaiz(Q,0,1000,0.01)

fprintf('\n(b) Calculado pela funcao fzero\n')
% (b) Calculado pela funcao fzero
Solucao = fzero(Q,400)