function Xs = SecanteRaiz(Fun,Xa,Xb,Err,imax)

%SecanteRaiz determina a raiz de Fun = 0 usnado o metodo da Secane.
%Variaveis de entreda:
%Fun Nome (string) da funcao que calcula Fun para um dado x.
%a,b Dois pontos na vizinhanca da raiz (ambos de um mesmo lado da raiz ou com a % raiz entre si).
%Err erro maximo.
%imax Numero maximo de iteracoes
%Variavel de saida
%Xs Solucao

	for i = 1:imax
		FunXb = feval(Fun,Xb);
		Xi = Xb - FunXb*(Xa - Xb)/(feval(Fun,Xa)-FunXb);
		if abs((Xi-Xb)/Xb) < Err
			Xs = Xi;
			break
		end
		Xa = Xb;
		Xb = Xi;
	end
	if i == imax
		fprintf(' A solucao nao foi obtida em %i iteractions. \n',imax)
		Xs = ('Sem resposta');
	end
