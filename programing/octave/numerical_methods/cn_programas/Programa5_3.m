clear all
clc
x=0:0.4:6 ;                 % Atribui os dados experimentas aos vetores x e y.
y=[0 3 4.5 5.8 5.9 5.8 6.2 7.4 9.6 15.6 20.7 26.7 31.1 35.6 39.3 41.5];
n=length(x);         % n e o numero de pontos m e a ordem do polinomio
m=15;
for i=1:2*m
  xsum(i)=sum(x.^(i));             % Define um vetor com os termos das somas das potencias de xi.
end
% Inicio do Passo 3                   % Atribui valores a primeira linha da matriz [a] e ao vetor [b].
a(1,1)=n;
b(1,1)=sum(y);
for j=2:m + 1
      a(1,j)=xsum(j-1);
end
for i = 2:m +1               % Cria linhas 2 a 5 da matriz [a] e elmentos 2 e 5 do vetor coluna [b]
  for j = 1:m+1
    a ( i , j ) = xsum ( j + i - 2) ;
  end
  b(i,1)= sum(x.^(i - 1) .*y) ;
end
% Passo 4
p = (a\b)                                     % Resolve o sistema [a][p]= = [b] para [p]. Transpõe a solução para que [p] se torne um vetor linha.
for i = 1:m+1
  Pcoef ( i) = p(m+2-i);         %Cria um novo vetor para os coeficientes do polinômio, a ser usado na função polyval do MATLAB (veja nota no final do exemplo).
end 
epsilon = 0:0.1:6;               %Define um vetor de deformações a ser usado no traçado do polinômio.                                                                               
stressfit = polyval (Pcoef, epsilon) ; %Tensão calculada pelo polinômio.
plot(x,y, 'ro' ,epsilon,stressfit, 'k', 'linewidth',2)
xlabel ('Deformação' , 'fontsize' , 20)
ylabel('Tensão (MPa) ','fontsize' ,20)



  
    