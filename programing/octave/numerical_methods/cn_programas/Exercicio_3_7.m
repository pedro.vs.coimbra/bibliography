clear
clc
  % Primeira parte do problema
x = -20:0.1:10;
F = inline('x.^3 + 12*x.^2 - 100*x - 6');
plot(x,F(x))                              
grid

 % Segunda parte do problema
 disp('Determinando a primerira raiz')
  
a = -18; b = -14; imax = 5; tol = 0.5;
Fa = F(a); Fb = F(b);
if Fa*Fb>0
  disp('Erro: A funcao tem o mesmo sinal nos pontos a e b.')
else 
  disp('  I        a          b    (xNS)Solucao  f(xNS)   Tolerancia')
  for i = 1:imax
    xNS = (a+b)/2;
    toli = (b - a)/2;
    FxNS = F(xNS);
    fprintf('%3i %11.6f%11.6f%11.6f%11.6f%11.6f\n', i, a, b, xNS, FxNS, toli)
     if FxNS==0
     fprintf(' Uma solucao exata x=%11.6f foi encontrada \m' , xNS)
     break
     end
     if toli < tol
     break
   end
   if i==imax
    fprintf(' Solucao nao foi obtida em %i iteracoes \n', imax)
    break
   end
   if F(a)*FxNS < 0
    b = xNS;
   else
    a = xNS;
   end
  end
 end
 
 % Terceira parte do problema
  disp('Determinando a segunda raiz')
  
a = -5; b = 3; imax = 5; tol = 0.5;
Fa = F(a); Fb = F(b);
if Fa*Fb>0
  disp('Erro: A funcao tem o mesmo sinal nos pontos a e b.')
else 
  disp('  I        a          b    (xNS)Solucao  f(xNS)   Tolerancia')
  for i = 1:imax
    xNS = (a+b)/2;
    toli = (b - a)/2;
    FxNS = F(xNS);
    fprintf('%3i %11.6f%11.6f%11.6f%11.6f%11.6f\n', i, a, b, xNS, FxNS, toli)
     if FxNS==0
     fprintf(' Uma solucao exata x=%11.6f foi encontrada \n' , xNS)
     break
     end
     if toli < tol
     break
   end
   if i==imax
    fprintf(' Solucao nao foi obtida em %i iteracoes \n', imax)
    break
   end
   if F(a)*FxNS < 0
    b = xNS;
   else
    a = xNS;
   end
  end
 end
 
  % Quarta parte do problema
  disp('Determinando a segunda raiz')
  
a = 5; b = 7.5; imax = 5; tol = 0.5;
Fa = F(a); Fb = F(b);
if Fa*Fb>0
  disp('Erro: A funcao tem o mesmo sinal nos pontos a e b.')
else 
  disp('  I        a          b    (xNS)Solucao  f(xNS)   Tolerancia')
  for i = 1:imax
    xNS = (a+b)/2;
    toli = (b - a)/2;
    FxNS = F(xNS);
    fprintf('%3i %11.6f%11.6f%11.6f%11.6f%11.6f\n', i, a, b, xNS, FxNS, toli)
     if FxNS==0
     fprintf(' Uma solucao exata x=%11.6f foi encontrada \n' , xNS)
     break
     end
     if toli < tol
     break
   end
   if i==imax
    fprintf(' Solucao nao foi obtida em %i iteracoes \n', imax)
    break
   end
   if F(a)*FxNS < 0
    b = xNS;
   else
    a = xNS;
   end
  end
 end

