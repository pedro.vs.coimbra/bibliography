# Basic Commands
## Movement
    k - move up
    l - move right
    j - move down
    h - move left

## Movement in visual line
    gk - move up
    gj - move down
    
## Movement in the screen
    <C-u> - Move half page up
    <C-d> - Move half page down
    L - Move to the low portion of the screen
    H - Move to the high portion of the screen
    M - Move the middle of the screen
    
## Additional Movement
    m{letter} - Place mark
    '{letter} - Go to mark
    gg - First line
    G - Last line
    [cont]G - Go to [cont] line
    <C-U> - Up 1/2 screen
    <C-D> - Down 1/2 screen
    z<Enter> - Make this line the top one on the screen
    zz - Center window around the current line 

## Inserting Text
    i - Before cursor
    I - Beginning of the line
    a - After cursor
    o - Line below cursor
    O - Live above cursor
    cw- Change word
    cc- Change entire line
    c{motion}- Change from cursor to motion

## Undo/Redo
    u - Undo the last change
    <C-R> - Redo the last change that was undone
    U - Undo all changes on the line

## Deleting text
    dd - Delete lines
    dw - Delete words
    x - Delete characters
    d{motion} Delete from cursor to {motion}

## Getting Out
    ZZ - Write file and exit
    :q! - Discard changes and exit
    ZQ - Discard changes and exit

## Help 
    :help{topic} - Display help on the given topic

## Searching
    f - Forward search for character on a line
    F - Backward search for character on a line
    / - Search forward for string
    ?{string} - Search backward for string
    n - Repeat last search
    N - Repeat last search in opposite direction
    \ - Repeat last search forward
    ? - Repeat last search backward

## Additional Editing Commands
    Editing
    . - Repeat last change
    [cont]["register] ..  d{motion} - Delete from cursor to {motion}
    dd - Delete lines
    c{motion} - Change from cursor to {motion}
    cc - change lines 
    y{motion} - Yank from cursor to {motion} 
    yy - Yank lines
    p - Put after cursor
    P - Put before cursor
    xp - Twiddle characters (giro de characters)
    [cont]r{char} - Replace on character
    [cont]R{string}<ESC> - Replace characters until stopped
    [cont]J - Join Lines 

## Windows
    :split {file} - Split window
    <C-Wj> - Go up a window
    <C-Wk> - Go down a window
    <C-Wo> - Make the current window the only one

## Keyboard Macros
    q{register} - Record commands into a {register}
    q - Stop recording
    :reg list all macros
    [count]@{register} - Execute the keyboard macro in {register}

## Text Formatting Commands
    [count]gq{motion} - Format selected text
    [count]gqq - Format lines
    gq} - Format Paragraphs
    :set textwidth={width} - Set the width of the text line (turn auto wrapping on)
    :[range]center - Center de text
    :[range]left - Left-align the text
    :[range]right - Right-align the text
    :set formatoptions={characters} - Set options which control formatting. 
    t - Automatically wrap text
    2 - When formatting text, use the second line of the paragraph to determine what indent to use.
    q - Allow formatting of comments with gq command


## Commands for Programmers
    % - Go to matching brace
    :set autoindent - Indent each new line the same as the previous one
    :set cindent - Use C style indentation
    <C-d> - In insert mode, no indent one level of auto indent
    [count]>> - Shift lines right
    [count]>{motion} - Shift right
    [count]<< - Shift lines left
    [count]<{motion} - Shift left
    [count]={motion} - Indent code from cursor to {motion}
    <C-]> - Jump to tag (function definition)
    [count]<C-t> - Go to location before last tab jump
    K - Run man on the word under the cursor

## Making the program
    :make - Run make and capture the output
    :cc - Jump to current error
    :cn - Jump to next error
    :cp - Jump to previous error

## Grep
    :grep '{string}'{files} - Run grep and capture the output. Treat matches like :make errors.

## Include File Searches
    [count] ] <C-D> - Finde the definition of the macho under the cursor
    [count] ] <C-I> - Search for the word under the cursor
    [count] ]d - List first macro definition for the macro the cursor is on.
    [count] ]D - List all macro definitions

## Commands for directory searches
    :set path={directory} - Tell Vim where to search for files
    :checkpath - Make sure that Vim can find all the fiels that are referenced in #include dirctives
    :find{file} - Edit the {file}. If the files is not in the current directory, search through the 'path' to find it.
    gt - Edit the file who's name is under the cursor. Search through the 'path' if file is not in the current directory.

# Visual Mode Commands
## Basic Commands
    v - Enter character visual~mode
    V - Enter line visual mode
    <C-V> - Enter block visual mode
    $ Move to the right side of the block to the end of each line

## Selection Commands
    [count]a( - Select () block
    [count]a[ - Select [] block
    [count]a[ - Select {} block
    [count]aw - Select word and space after
    [count]iw - Select word only
    [count]as - Select sentence and following space
    [count]is - Select sentence only

## Editing Commands
    ["register]d - Delete the selected text
    ["register]y - Yank the text into register
    > - Shift text right
    < - Shift text left
    = - Run the lines through th indent program
    J - Join the highlighted lines
    gq{motion} - Format text to 'textwidth'
    U - Convert text to all UPPER CASE
    u Conver text to all lower case
    ~ - Invert the case of the text.

# Insert Mode Commands
## Insert Mode Commands
    <C-V>{char} - Insert character literally
    <C-D> - Unindent one level
    0<C-D> - Remove all automatic indentation
    <C-t> - insert one 'Shiftwidth'
    <C-y> - Copy the character above the cursor
    <C-e> - Copy the character below the cursor
    <C-N> - Find next match of the 
    <C-P> - Find previous match for word before the cursor
    <C-R>{register} - Insert contents of a register
    <C-K>{char1}{char2} - Insert digraphs
    <C-W> - Delete word before cursor
  
## Abbreviations 
:abbreviate {abbr}{expansion} - Define abbreviation

# Ex Mode Commands
## Basic Commands
    Q Enter Ex mode 
    :vi Enter normal mode
    :[range]s/{old}/{new}/{flags} - Substitue {new} for {old}. If a flag of "g" is present substitue all occorrences on a line. Otherwise just the first one.
    :[range]write[file] - Write text to a file
    :[line]read[file] - Read text from another file into the current file

## File Selection Commands
    :rewind - Edit the first file
    :next - Edit next file
    :prev - Edit previous file
    :last - Edit last filein the list
    :args - List the files in the edit list

## Editing
    :[range]delete - Delete teh specified lines
    :[range]copy[line] - Copy specified lines to [line] (default = afiter current line)
    :[range]move[line] - Like :copy but delete the lines as well.

## Miscellaneous Commands
    :[renge]print - Print the specified lines
    :[range]number - Print the specified lines with line numbers
    :[range]list - Print the specified liens with 'list' option on
    :dlist{name} - List definitions for {name}
    :[range]retab[!]{tabstop} - Change the tabbing from the existing setting to the new tabstop}
    :exti[!] - Close the current file. If it's the last one, exit the editor
    :suspend - Suspend the editor
    :source{file} - Read commands from the specified file
    :redir>{fiel} - Record the output the commands in the specified file

# Options
## Setting
    :set{option}={value} - Set an option
    :set{option}? - Display the value of an option
    :set - Display the value of options that are set to something other than default
    :set all - Display the value of all options
    :set{option}& - Set an options to the default
    :browse set - Set an option using a full screen based dialog.
    :set {option} +={value} - Add a value toa a list of options (string option). Add a number to a numeric option.
    :set {option} -={value} - Remove a value grom a list of options (string option). Subtract a number from a numeric option.
    :set{option} - Turn on a boolean option
    :set no {option} - Turn off a boolean option
    :set inv{option} - Invert a boolean option

# Indent/Tabbin
    :set cindent - Turn on C style indentation
    :set autoindent - Indent each line the same as the previous one
    :set expnadtabs - Trun all tabs into space
    :set softtabstop - Set the amount of space that the <Tab> key uses.(Note: This is not the same as the number of spaces for a Tab.)

## Listing Options
    :set list - Turn on list mode where everything is visible
    :set number - Display line numbers

## Searching Options
    :set hlsearch - Highlight matching search strings
    :set incsearch - Do incremental searches
    :set wrapscan - If a search reaches the bottom of the file wrap to the top and keep searching. (Also will wrap from the top to the bottom for reverse searches)
    :set ignorecase - Make searches case insensitive
    :set autowrite - Automatically write files as needed to preserve data

# Regular expressions
## Simple Atoms
    x - The literal character "x"
    ^ - Star of line
    $ - End of line
    . - A single character
    \< - Star of a word
    \> - and of word

# Range Atoms
    [abc] - Match either "a","b", or "c"
    [^abc] - Match aything except "a","b", or "c"
    [a-z] - Match all charactets from "a" through "z"
    [a-zA-Z] - Match all characters from "a" trough "z" and "A" through "Z"

# Sub Patterns
    \(pattern\) - Mark the pattern for later use. The first set of \(.\) marks a pattern as \1, the second as \2 and so on
    \1 - Matchs the same string that was matched by the first sub-expression in \(and\). Exemple: "\([a-z]\).\1" matches "ata", "ehe","tot".

# Modifiers
    * - Match the previous atom 0 or more times. As much as possible
    \+- Match the previous atom 1 or more times. As much as possible
    \=- Match the previous atom 0 or 1 times
    \{}-Match the previous atom 0 or more times. (Same as "*" modifies)
    \{n}- Match the  previous atom n times
    \{n,m} - Match the previous atom n to m times
    \{n,} - Match the previous atom n or more times
    \{,m} - Match the previous atom from 0 to m times
   \{-n,m} - Mathc the prievious atom n to m times. Match as little as possible
   \{-n,} - Match the previous atom at least n times. Match as little as possible
   \{-,m} - Match the previous atom up to m times. Math as little as possible
   \{-} - Match the previous atom 0 or more times. Match as little as possible
   str1\str2 - Match str1 or str2


:set spell!
"]s - goes to the misspell word
suggestions for sell - z=
add to bad word list - zw remove - zuw



# Editar do mesmo ponto que parou gi

<C-x> - completation mode
Olhar h index

# Folding
  zf#j creates a fold from the cursor down # lines.
  zf/string creates a fold from the cursor to string.
  zj moves the cursor to the next fold.
  zk moves the cursor to the previous fold.
  zo opens a fold at the cursor.
  zO opens all folds at the cursor.
  zm increases the foldlevel by one.
  zM closes all open folds.
  zr decreases the foldlevel by one.
  zR decreases the foldlevel to zero -- all folds will be open.
  zd deletes the fold at the cursor.
  zE deletes all folds.
  [z move to start of open fold.
  ]z move to end of open fold.
