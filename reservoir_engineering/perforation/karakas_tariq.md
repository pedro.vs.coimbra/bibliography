São apresentados dois casos um caso 2D plano válido para pequenas dimensões e
um problema 3D.

$s_t = s_P + s\_{dp}$

s_P = eficiencia relativa de um poço quando comparado a um buraco aberto
ideal.

s\textsubscript{sp} = treatable skin in perforated completions
